function actualizarmonto(){
    var monto_total = 0;
    
    $( ".tr-venta-detalle" ).each(function() {
        var id_detalle_venta = $(this).attr("data-id");
        var cantidad_detalle_venta = $(".td-venta-cant[data-id="+id_detalle_venta+"]").val();
        var valor_detalle_venta = $(".td-venta-punit[data-id="+id_detalle_venta+"]").html();
        var total_detalle_venta = parseFloat(valor_detalle_venta) * parseInt(cantidad_detalle_venta);
        
        monto_total+=total_detalle_venta;
    });
    var descuento_act = $("#descuento-venta").val();
    if(!descuento_act){
       descuento_act=0;
    }
    monto_total-=parseFloat(descuento_act);
    $(".td-venta-montotot").html(monto_total.toFixed(2));
}

function actualizarsubmonto(id_detalle_venta){
    var cantidad_detalle_venta = $(".td-venta-cant[data-id="+id_detalle_venta+"]").val();
    var valor_detalle_venta = $(".td-venta-punit[data-id="+id_detalle_venta+"]").html();
    var valor_detalle_compra = $(".td-compra-punit[data-id="+id_detalle_venta+"]").html();

     var total_detalle_venta = parseFloat(valor_detalle_venta) * parseInt(cantidad_detalle_venta);
     $(".td-venta-ptot[data-id="+id_detalle_venta+"]").text( total_detalle_venta.toFixed(2) );
    
     var total_detalle_compra = parseFloat(valor_detalle_compra) * parseInt(cantidad_detalle_venta);
     $(".td-compra-ptot[data-id="+id_detalle_venta+"]").text( total_detalle_compra.toFixed(2) );
}

function actualizarvuelto(){
    var monto_actual = $("#td-venta-montotot").html();
    var retencion_act = $("#retencion").val();
    var descuento_act = $("#descuento-venta").val();
    
    if(!retencion_act){
       retencion_act=0;
    }
    if(!descuento_act){
       descuento_act=0;
    }
    var total_vuelto_venta = parseFloat($("#paga-venta").val()) - parseFloat(monto_actual) - parseFloat(retencion_act);
    if(total_vuelto_venta){
        $(".td-venta-vueltomonto").html(total_vuelto_venta.toFixed(2));
    }
    console.log("se actualizó");
}

$(document).on('change', '.td-venta-cant', function() {
    var id_detalle_venta = $(this).attr("data-id");
    var cantidad_detalle_venta = $(this).val();
    var valor_detalle_venta = $(".td-venta-punit[data-id="+id_detalle_venta+"]").html();
    var valor_detalle_compra = $(".td-compra-punit[data-id="+id_detalle_venta+"]").html();
    var codigo_detalle_venta = $(".td-venta-cod[data-id="+id_detalle_venta+"]").html();

     var total_detalle_venta = parseFloat(valor_detalle_venta) * parseInt(cantidad_detalle_venta);
     var total_detalle_compra = parseFloat(valor_detalle_compra) * parseInt(cantidad_detalle_venta);
    
     $(".td-venta-ptot[data-id="+id_detalle_venta+"]").text( total_detalle_venta.toFixed(2) );
     $(".td-compra-ptot[data-id="+id_detalle_venta+"]").text( total_detalle_compra.toFixed(2) );
    
    actualizarmonto();
    actualizarvuelto();
    
    console.log(codigo_detalle_venta);
    console.log(id_detalle_venta);
    console.log(cantidad_detalle_venta);
    console.log(valor_detalle_venta);
});

$(document).on('click', '.td-venta-elim', function() {
    var id_detalle_venta = $(this).attr("data-id");
    $(".tr-venta-detalle[data-id="+id_detalle_venta+"]").remove();
    console.log("Se eliminó");
    actualizarmonto();
    actualizarvuelto();
});

$(document).on('input change', '#paga-venta', function() {
    actualizarvuelto();
    
});

$(document).on('input change', '#retencion', function() {
    actualizarvuelto();
    
});
$(document).on('input change', '#descuento-venta', function() {
    actualizarmonto();
    actualizarvuelto();
});

$(document).on('change', '.input-venta-sub', function() {
    var id_detalle_venta = $(this).attr("data-id");
    var codigo_subproducto = $(this).attr("data-subprod");
    var valor_precio = parseFloat($(".td-venta-punit[data-id="+id_detalle_venta+"]").attr("data-precio")).toFixed(2);
    var valor_subprecio = parseFloat($(".td-venta-punit[data-id="+id_detalle_venta+"]").attr("data-subprecio")).toFixed(2);
    
    var valor_compra = parseFloat($(".td-compra-punit[data-id="+id_detalle_venta+"]").attr("data-precio")).toFixed(2);
    var valor_subcompra = parseFloat($(".td-compra-punit[data-id="+id_detalle_venta+"]").attr("data-subprecio")).toFixed(2);
    
    
    if(this.checked && (valor_subprecio == 0)) {
        var codigo_interno ="";
        var codigo_interno_subproducto ="";
        var id_inventario =0;
        var precio_venta =0;
        var descuento_venta =0;
        var precio_venta_final =0;
        var nuevo_elemento_tr = "";

        var result = $.grep(myjson.producto, function(element, index) {
           return (element.cod_interno == codigo_subproducto);
        });
        if(result[0]){
            precio_venta =result[0].prec_venta_producto;
            precio_compra =result[0].prec_compra_producto;
            descuento_venta =result[0].descuento_producto;

            precio_venta_final=parseFloat(precio_venta) -parseFloat(descuento_venta);
            precio_venta_final=precio_venta_final.toFixed(2);

            $(".td-venta-punit[data-id="+id_detalle_venta+"]").html(precio_venta_final);
            $(".td-venta-punit[data-id="+id_detalle_venta+"]").attr("data-subprecio", precio_venta_final);
            
            $(".td-compra-punit[data-id="+id_detalle_venta+"]").html(precio_compra);
            $(".td-compra-punit[data-id="+id_detalle_venta+"]").attr("data-subprecio", precio_compra);

            actualizarsubmonto(id_detalle_venta);
            actualizarmonto();
            actualizarvuelto();

        }else{
            console.log("No se encontró producto");
                }
    }else if(this.checked && (valor_subprecio > 0)){
        
        $(".td-venta-punit[data-id="+id_detalle_venta+"]").html(valor_subprecio); 
        $(".td-compra-punit[data-id="+id_detalle_venta+"]").html(valor_subcompra);    
        actualizarsubmonto(id_detalle_venta);
        actualizarmonto();
        actualizarvuelto();
        
    }else if(!(this.checked)){
        
        $(".td-venta-punit[data-id="+id_detalle_venta+"]").html(valor_precio);  
        $(".td-compra-punit[data-id="+id_detalle_venta+"]").html(valor_compra);    
        actualizarsubmonto(id_detalle_venta);
        actualizarmonto();
        actualizarvuelto();
    
    }
    
});

function verificarefecha(ano, mes,tienda){
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var day = d.getDate();
    
    /*
    console.log(ano);
    console.log(mes);
    console.log(year);
    console.log(month);
    console.log(day);
    */
    
    if((ano < 2018) || (ano >= 2100)){
       alert("ingrese un año válido.");
    }else if((ano > year) || ((ano = year) && (mes >= month))){
       alert("ingrese una fecha que ya haya pasado.");
    }else{
        window.location.href = "historial-gp.php?xano="+ano+"&xmes="+mes+"&xtienda="+tienda;
    }
    
    
    
}