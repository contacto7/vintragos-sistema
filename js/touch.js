jQuery(function($) {

	// QWERTY Text Input
	// The bottom of this file is where the autocomplete extension is added
	// ********************
	$('#cont-adm').keyboard({ layout: 'qwerty' });
	$('#adicional').keyboard({ layout: 'qwerty' });
	$('#nuevo-prod').keyboard({ layout: 'qwerty' });

	// Contenteditable
	// ********************
	$.keyboard.keyaction.undo = function (base) {
		base.execCommand('undo');
		return false;
	};
	$.keyboard.keyaction.redo = function (base) {
		base.execCommand('redo');
		return false;
	};

	$('#contenteditable').keyboard({
		usePreview: false,
		useCombos: false,
		autoAccept: true,
		layout: 'custom',
		customLayout: {
			'normal': [
				'` 1 2 3 4 5 6 7 8 9 0 - = {del} {b}',
				'{tab} q w e r t y u i o p [ ] \\',
				'a s d f g h j k l ; \' {enter}',
				'{shift} z x c v b n m , . / {shift}',
				'{accept} {space} {left} {right} {undo:Undo} {redo:Redo}'
			],
			'shift': [
				'~ ! @ # $ % ^ & * ( ) _ + {del} {b}',
				'{tab} Q W E R T Y U I O P { } |',
				'A S D F G H J K L : " {enter}',
				'{shift} Z X C V B N M < > ? {shift}',
				'{accept} {space} {left} {right} {undo:Undo} {redo:Redo}'
			]
		},
		display: {
			del: '\u2326:Delete',
			redo: '↻',
			undo: '↺'
		}
	});


	// Num Pad Input
	// ********************
	$('#paga-venta').keyboard({
		layout: 'num',
		restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
		preventPaste : true,  // prevent ctrl-v and right click
		autoAccept : true
	});
	// Num Pad Input
	// ********************
	$('#descuento-venta').keyboard({
		layout: 'num',
		restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
		preventPaste : true,  // prevent ctrl-v and right click
		autoAccept : true
	});
	$('#retencion').keyboard({
		layout: 'num',
		restrictInput : true, // Prevent keys not in the displayed keyboard from being typed in
		preventPaste : true,  // prevent ctrl-v and right click
		autoAccept : true
	});



/* example from readme, comment out the meta example above then uncomment this one to test it
	$('#meta').keyboard({
		layout: 'custom',
		display: {
			'meta1'  : '\u2666', // Diamond
			'meta2'  : '\u2665'  // Heart
		},
		customLayout: {
			'normal' : [
				'd e f a u l t',
				'{meta1} {meta2} {accept} {cancel}'
			],
			'meta1' : [
				'm y m e t a 1',
				'{meta1} {meta2} {accept} {cancel}'
			],
			'meta2' : [
				'M Y M E T A 2',
				'{meta1} {meta2} {accept} {cancel}'
			]
		}
	})
*/


	// Console showing callback messages
	// ********************
	$('.ui-keyboard-input').bind('visible hidden beforeClose accepted canceled restricted', function(e, keyboard, el, status){
        
            //console.log(e, keyboard, el.id, status);
            //$( "#"+el.id ).trigger( "input" );
			switch (e.type){
				case 'visible'  :
                    break;
				case 'hidden'   : 
                    
                    break;
				case 'accepted' : 
                    //console.log(e, keyboard, el.id, status);
                    //$( "#"+el.id ).trigger( "input" );
                    actualizarvuelto();
                    break;
				case 'canceled' : 
                    
                    break;
				case 'restricted'  : 
                    
                    break;
				case 'beforeClose' : 
                    
                    break;
			}
        
	});

	// Show code
	// ********************
	$('h2 span').click(function(){
		var orig = 'Click, then scroll down to see this code',
			active = 'Scroll down to see the code for this example',
			t = '<h3>' + $(this).parent().text() + ' Code</h3>' + $(this).closest('.block').find('.code').html();
		// add indicator & update tooltips
		$('h2 span')
			.attr({ title : orig, 'original-title': orig })
			.parent()
			.filter('.active')
			.removeClass('active');
		$(this)
			.attr({ title : active, 'original-title': active })
			// hide, then show the tooltip to force updating & realignment
			.tipsy('hide')
			.tipsy('show')
			.parent().addClass('active');
		$('#showcode').html(t).show();
	});


// ********************
// Extension demos
// ********************

	// Set up typing simulator extension on ALL keyboards
	$('.ui-keyboard-input').addTyping();

	// simulate typing into the keyboard
	// \t or {t} = tab, \b or {b} = backspace, \r or \n or {e} = enter
	// added {l} = caret left, {r} = caret right & {d} = delete
	$('#inter-type').click(function(){
		$('#inter').getkeyboard().reveal().typeIn("{t}Hal{l}{l}{d}e{r}{r}l'o{b}o {e}{t}World", 500);
		return false;
	});
	$('#meta-type').click(function(){
		var meta = $('#meta').getkeyboard();
		meta.reveal().typeIn('aBcD1112389\u0411\u2648\u2649', 700, function(){ meta.accept(); alert('all done!'); });
		return false;
	});
    

});