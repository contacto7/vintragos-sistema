function  seleclistaserv(e){
    $("[id^='descr-serv-']").removeClass("descr-serv-active");
    $("[id^='listaserv-']").removeClass("listaserv-active");
    
    for (i = 1; i <= 3; i++) { 
        if(e == i){
            $("#listaserv-"+i).removeClass("listaserv-a");
            $("#listaserv-"+i).addClass("listaserv-active");
            
            $("#descr-serv-"+i).removeClass("descr-serv");
            $("#descr-serv-"+i).addClass("descr-serv-active");



        }else{
            if(!$("#listaserv-"+i).hasClass("listaserv-a")){
                $("#listaserv-"+i).addClass("listaserv-a");
            }
            if(!$("#descr-serv-"+i).hasClass("descr-serv")){
                $("#descr-serv-"+i).addClass("descr-serv");
            }
        }
    }
    
    //Pongo los divs al final
    for (i = 1; i <= 3; i++) { 
        if(e != i){

            $("#descr-serv-"+i).detach().appendTo('.descr-serv-inn')
        }
    }
}

$(".lista-nav").click(function() {
    var idir=$(this).attr("data-idref");

    $('html, body').animate({
        scrollTop: $("#"+idir).offset().top-200
    }, 1000);
});

$(".circimg-ino").click(function() {
    $('html, body').animate({
        scrollTop: $('body').offset().top
    }, 1000);
});

function verificarnombre(){
    if($("#nombre-cotiza").val()==''){
        $("#nombre-form-wrap").removeClass("has-success");
        $("#nombre-cotiza").removeClass("form-control-success");
        $("#nombre-form-wrap").addClass("has-danger");
        $("#nombre-cotiza").addClass("form-control-danger");
        console.log();
    }else{
        $("#nombre-form-wrap").removeClass("has-danger");
        $("#nombre-cotiza").removeClass("form-control-danger");
        $("#nombre-form-wrap").addClass("has-success");
        $("#nombre-cotiza").addClass("form-control-success");
    }
}

function verificarcorreo(){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var escorreo=regex.test($("#email-cotiza").val());
    
    if(!escorreo){
        $("#email-form-wrap").removeClass("has-success");
        $("#email-cotiza").removeClass("form-control-success");
        $("#email-form-wrap").addClass("has-danger");
        $("#email-cotiza").addClass("form-control-danger");
        console.log();
    }else{
        $("#email-form-wrap").removeClass("has-danger");
        $("#email-cotiza").removeClass("form-control-danger");
        $("#email-form-wrap").addClass("has-success");
        $("#email-cotiza").addClass("form-control-success");
    }
}

function verificarnumero(){
    if($("#numero-cotiza").val().length<9){
        $("#numero-form-wrap").removeClass("has-success");
        $("#numero-cotiza").removeClass("form-control-success");
        $("#numero-form-wrap").addClass("has-danger");
        $("#numero-cotiza").addClass("form-control-danger");
        console.log();
    }else{
        $("#numero-form-wrap").removeClass("has-danger");
        $("#numero-cotiza").removeClass("form-control-danger");
        $("#numero-form-wrap").addClass("has-success");
        $("#numero-cotiza").addClass("form-control-success");
    }
}

function verificartexto(){
    if($("#texto-cotiza").val()==''){
        $("#texto-form-wrap").removeClass("has-success");
        $("#texto-cotiza").removeClass("form-control-success");
        $("#texto-form-wrap").addClass("has-danger");
        $("#texto-cotiza").addClass("form-control-danger");
        console.log();
    }else{
        $("#texto-form-wrap").removeClass("has-danger");
        $("#texto-cotiza").removeClass("form-control-danger");
        $("#texto-form-wrap").addClass("has-success");
        $("#texto-cotiza").addClass("form-control-success");
    }
}

$( "#nombre-cotiza" ).click(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "0"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#nombre-cotiza" ).focusin(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "0"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#email-cotiza" ).click(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "40px"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#email-cotiza" ).focusin(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "40px"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#numero-cotiza" ).click(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "80px"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#numero-cotiza" ).focusin(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "80px"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#texto-cotiza" ).click(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "120px"
  }, 500, function() {
    // Animation complete.
  });
});

$( "#texto-cotiza" ).focusin(function() {
  $( ".flecha-ind-wrapp" ).animate({
    top: "120px"
  }, 500, function() {
    // Animation complete.
  });
});

$('.descr-serv-inn .carousel').carousel({
    interval: 7000
});

function movertestizq(){
    $("#clientes-test-row").css( "margin-left", "+=50" );

}

function movertestder(){
    $("#clientes-test-row").css( "margin-left", "-=50" );
    $(".cli-fle-izq").show();
}
