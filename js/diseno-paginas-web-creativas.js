function verificarnombre(elem1, elem2){
    var id_camb_1="#"+elem1;
    var id_camb_2="#"+elem2;
    
    if($(id_camb_1).val()==''){
        $(id_camb_2).removeClass("has-success");
        $(id_camb_1).removeClass("form-control-success");
        $(id_camb_2).addClass("has-danger");
        $(id_camb_1).addClass("form-control-danger");
    }else{
        $(id_camb_2).removeClass("has-danger");
        $(id_camb_1).removeClass("form-control-danger");
        $(id_camb_2).addClass("has-success");
        $(id_camb_1).addClass("form-control-success");
    }
}

function verificarcorreo(elem1, elem2){
    var id_camb_1="#"+elem1;
    var id_camb_2="#"+elem2;
    
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var escorreo=regex.test($(id_camb_1).val());
    
    if(!escorreo){
        $(id_camb_2).removeClass("has-success");
        $(id_camb_1).removeClass("form-control-success");
        $(id_camb_2).addClass("has-danger");
        $(id_camb_1).addClass("form-control-danger");
    }else{
        $(id_camb_2).removeClass("has-danger");
        $(id_camb_1).removeClass("form-control-danger");
        $(id_camb_2).addClass("has-success");
        $(id_camb_1).addClass("form-control-success");
    }
}

function verificarnumero(elem1, elem2){
    var id_camb_1="#"+elem1;
    var id_camb_2="#"+elem2;
    
    if($(id_camb_1).val().length<9){
        $(id_camb_2).removeClass("has-success");
        $(id_camb_1).removeClass("form-control-success");
        $(id_camb_2).addClass("has-danger");
        $(id_camb_1).addClass("form-control-danger");
    }else{
        $(id_camb_2).removeClass("has-danger");
        $(id_camb_1).removeClass("form-control-danger");
        $(id_camb_2).addClass("has-success");
        $(id_camb_1).addClass("form-control-success");
    }
}

function verificartexto(elem1, elem2){
    var id_camb_1="#"+elem1;
    var id_camb_2="#"+elem2;
    
    if($(id_camb_1).val()==''){
        $(id_camb_2).removeClass("has-success");
        $(id_camb_1).removeClass("form-control-success");
        $(id_camb_2).addClass("has-danger");
        $(id_camb_1).addClass("form-control-danger");
        console.log();
    }else{
        $(id_camb_2).removeClass("has-danger");
        $(id_camb_1).removeClass("form-control-danger");
        $(id_camb_2).addClass("has-success");
        $(id_camb_1).addClass("form-control-success");
    }
}
function toggdscrcot(){

    if($('#dscr-cotiza').prop('checked')){
        $("#texto-form-wrap").show();
    }
    else {
        $("#texto-form-wrap").hide();
        var p = $( ".flecha-ind-wrapp:first" );
        var position = p.position();
        var pos_top = position.top;
        
        if(pos_top == 150){
          $( ".flecha-ind-wrapp" ).animate({
            top: "80px"
          }, 500, function() {
            // Animation complete.
          });
        }
        

    }
}
function toggdscrcot(e){
    var estado_txt = $(e).attr("data-esta");
    
    if(estado_txt == "0"){
        $(e).attr("data-esta", "1");
        //$("#texto-form-wrap").show();
        
        $elie = $("#flecha-dscridea");
        
        $elie.animateRotate(0,90, 250, 'linear', function () { $("#texto-form-wrap").show() });

    }
    else {
        $(e).attr("data-esta", "0");
        
        //$("#texto-form-wrap").hide();
        
        $elie = $("#flecha-dscridea");
        
        $elie.animateRotate(90,0, 250, 'linear', function () { $("#texto-form-wrap").hide() });
        
        
        

    }
}

$.fn.animateRotate = function(startAngle, endAngle, duration, easing, complete){
    return this.each(function(){
        var elem = $(this);

        $({deg: startAngle}).animate({deg: endAngle}, {
            duration: duration,
            easing: easing,
            step: function(now){
                elem.css({
                  '-moz-transform':'rotate('+now+'deg)',
                  '-webkit-transform':'rotate('+now+'deg)',
                  '-o-transform':'rotate('+now+'deg)',
                  '-ms-transform':'rotate('+now+'deg)',
                  'transform':'rotate('+now+'deg)'
                });
            },
            complete: complete || $.noop
        });
    });
};

$( ".foot-cuadr-tit" ).click(function(e) {
    var estado = $(e.target).attr("data-state");
    $elie = $("#flecha-cont-state");
    
    if( estado == "0"){
        $( ".foot-cuadr-inn" ).animate({
            bottom: 0,
        }, 500, function() {
            // Animation complete.
        });
        $(e.target).attr("data-state","1");
        
        $elie.animateRotate(0,180, 250, 'linear', function () {});
    }else{
        $( ".foot-cuadr-inn" ).animate({
            bottom: "-94px",
        }, 500, function() {
            // Animation complete.
        });
        $(e.target).attr("data-state","0");
        
        $elie.animateRotate(180,0, 250, 'linear', function () {});
    }
});