function verificarnombre(){
    if($("#nombre-cotiza").val()==''){
        $("#nombre-form-wrap").removeClass("has-success");
        $("#nombre-cotiza").removeClass("form-control-success");
        $("#nombre-form-wrap").addClass("has-danger");
        $("#nombre-cotiza").addClass("form-control-danger");
        console.log();
        
        corr_info_nombre = 0;
        
    }else{
        
        corr_info_nombre = 1;
        
        $("#nombre-form-wrap").removeClass("has-danger");
        $("#nombre-cotiza").removeClass("form-control-danger");
        $("#nombre-form-wrap").addClass("has-success");
        $("#nombre-cotiza").addClass("form-control-success");
        
        //PARTE DEL PREVIEW
        $(".preview-dv-1").animate({
            opacity: 1,
          }, 500, function() {
            // Animation complete.
          });
        $("#prev-nombre").html($("#nombre-cotiza").val());
    }
}

function verificarcorreo(){
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var escorreo=regex.test($("#email-cotiza").val());
    
    if(!escorreo){
        $("#email-form-wrap").removeClass("has-success");
        $("#email-cotiza").removeClass("form-control-success");
        $("#email-form-wrap").addClass("has-danger");
        $("#email-cotiza").addClass("form-control-danger");
        console.log();
        
        corr_info_correo = 0;
        
    }else{
        
        corr_info_correo = 1;
        
        $("#email-form-wrap").removeClass("has-danger");
        $("#email-cotiza").removeClass("form-control-danger");
        $("#email-form-wrap").addClass("has-success");
        $("#email-cotiza").addClass("form-control-success");
        
        //PARTE DEL PREVIEW
        $(".preview-dv-2").animate({
            opacity: 1,
          }, 500, function() {
            // Animation complete.
          });
        $("#prev-mail").html($("#email-cotiza").val());
    }
}

function verificarnumero(){
    if($("#numero-cotiza").val().length<9){
        $("#numero-form-wrap").removeClass("has-success");
        $("#numero-cotiza").removeClass("form-control-success");
        $("#numero-form-wrap").addClass("has-danger");
        $("#numero-cotiza").addClass("form-control-danger");
        console.log();
        
        corr_info_numero = 0;
        
    }else{
        
        corr_info_numero = 1;
        
        $("#numero-form-wrap").removeClass("has-danger");
        $("#numero-cotiza").removeClass("form-control-danger");
        $("#numero-form-wrap").addClass("has-success");
        $("#numero-cotiza").addClass("form-control-success");
        
        //PARTE DEL PREVIEW
        $(".preview-dv-3").animate({
            opacity: 1,
          }, 500, function() {
            // Animation complete.
          });
        $("#prev-cel").html($("#numero-cotiza").val());
    }
}

function verificartexto(){
    if($("#texto-cotiza").val()==''){
        $("#texto-form-wrap").removeClass("has-success");
        $("#texto-cotiza").removeClass("form-control-success");
        $("#texto-form-wrap").addClass("has-danger");
        $("#texto-cotiza").addClass("form-control-danger");
        console.log();
        
        $("#prev-dscr").html($("#numero-cotiza").val());
    }else{
        $("#texto-form-wrap").removeClass("has-danger");
        $("#texto-cotiza").removeClass("form-control-danger");
        $("#texto-form-wrap").addClass("has-success");
        $("#texto-cotiza").addClass("form-control-success");
        
        $("#prev-dscr").html($("#numero-cotiza").val());
    }
}
function toggdscrcot(){

    if($('#dscr-cotiza').prop('checked')){
        $("#texto-form-wrap").show();
    }
    else {
        $("#texto-form-wrap").hide();
        var p = $( ".flecha-ind-wrapp:first" );
        var position = p.position();
        var pos_top = position.top;
        
        if(pos_top == 150){
          $( ".flecha-ind-wrapp" ).animate({
            top: "80px"
          }, 500, function() {
            // Animation complete.
          });
        }
        

    }
}
function toggdscrcot(e){
    var estado_txt = $(e).attr("data-esta");
    
    if(estado_txt == "0"){
        $(e).attr("data-esta", "1");
        //$("#texto-form-wrap").show();
        
        $elie = $("#flecha-dscridea");
        
        $elie.animateRotate(0,90, 250, 'linear', function () { $("#texto-form-wrap").show() });

    }
    else {
        $(e).attr("data-esta", "0");
        
        //$("#texto-form-wrap").hide();
        
        $elie = $("#flecha-dscridea");
        
        $elie.animateRotate(90,0, 250, 'linear', function () { $("#texto-form-wrap").hide() });
        
        
        

    }
}

$.fn.animateRotate = function(startAngle, endAngle, duration, easing, complete){
    return this.each(function(){
        var elem = $(this);

        $({deg: startAngle}).animate({deg: endAngle}, {
            duration: duration,
            easing: easing,
            step: function(now){
                elem.css({
                  '-moz-transform':'rotate('+now+'deg)',
                  '-webkit-transform':'rotate('+now+'deg)',
                  '-o-transform':'rotate('+now+'deg)',
                  '-ms-transform':'rotate('+now+'deg)',
                  'transform':'rotate('+now+'deg)'
                });
            },
            complete: complete || $.noop
        });
    });
};