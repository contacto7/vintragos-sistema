<!DOCTYPE html>
<html ng-app="">
<body>
<footer class="foot-wrap-in">
    

    <div class="izq-foot-inn"> 
        <div class="cir-grand"> </div>
        <div class="logo-foot-wrapp">
            <img class="logo-foot" src="img/Logo-morado.jpg" alt="logo en el footer">  
        </div>
    </div>

    
    <div class="dere-foot-wrap">
        <div class="dere-foot-inn">
            <div class="dere-izq-foot">
                <div class="der-izq-tit">
                    <span>CONTACTO</span>
                </div>
                <div class="der-izq-dscr">
                    <div class="der-izq-sp">
                        <a class="foot-img-wrapp" href="https://api.whatsapp.com/send?phone=+51961518524&text=Hola%20Heser%20de%20Inoloop!%20estoy%20interesado%20en%20sus%20servicios" target="_blank">
                            <img src="img/foot-wa.png" alt="whatsapp de la empresa">
                        </a>
                        <a class="foot-img-wrapp" href="tel:+51961518524">
                            <img src="img/celular.png" alt="llamada de la empresa">
                        </a>
                        <span class="foot-dscr-let">
                            (+51) 961518524
                        </span>
                    </div>
                    <div class="der-izq-sp">
                        <a class="foot-img-wrapp" href="mailto:contacto@inoloop.com" target="_blank">
                            <img src="img/foot-mail.png" alt="email de la empresa">
                        </a>
                        <span class="foot-dscr-let">
                            contacto@inoloop.com
                        </span>
                    </div>
                </div>
            </div><!-- 
            --><div class="dere-der-foot">
                <div class="der-izq-tit">
                    <span>SÍGUENOS EN</span>
                </div>
                <div class="der-izq-dscr">
                    <div class="der-izq-sp">
                        <a class="foot-img-wrapp" href="https://www.facebook.com/inoloop" target="_blank">
                            <img src="img/foot-fb.png" alt="perfil de facebook de la empresa">
                        </a>
                        <span class="foot-dscr-let">
                            /Inoloop
                        </span>
                    </div>
                    <div class="der-izq-sp">
                        <a class="foot-img-wrapp" href="https://www.youtube.com/channel/UC4SnSTzExE4Qt6xTo2RVQvA/videos" target="_blank">
                            <img src="img/foot-yt.png" alt="canal de youtube de la empresa">
                        </a>
                        <span class="foot-dscr-let">
                            Inoloop
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="abajo-foot-wrapp">
        <div class="abajo-foot-inn">
            <span>&#64; 2017 INOLOOP SAC</span>
        </div>
    </div>
</footer>

</body>
</html>

