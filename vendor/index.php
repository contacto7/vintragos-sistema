<?php

include 'funciones.php';
?>


<!DOCTYPE html>
<html ng-app="">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inoloop</title>
    <link rel="icon" href="img/logo-chiqui.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/bootstrap-social.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/jquery.raty.css">
    <link href="css/princ.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery.min.older.js"></script>
</head>
<body>
<!-- 
LO QUE SE VE NO SE INSPECCIONA 
~(˘▾˘~) (~˘▾˘)~
-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<nav class="navbar navbar-nav navbar-fixed-top" role="navigation">

	<div class="container">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar,#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
		</div>
        <div class="imghead-wrap-2"><img src="img/Header.png" id="imagen-backhead"></div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
            <div class="imghead-wrap"><img src="img/Header.png" id="imagen-backhead"></div>
			<ul class="nav navbar-nav navbar-middle-ino">
 				<li class="elemli-nav">
                    <a href="javascript:void(0);" class="lista-nav" data-idref="servicios"><span>Servicios</span></a>
                </li>
 				<li class="elemli-nav">
                    <a href="javascript:void(0);" class="lista-nav" data-idref="cotiza"><span>Cotiza</span></a>
                </li>
 				<li class="elemli-nav" id="elemli-circ">
                    <a href="javascript:void(0);" class="circimg-ino"><img class="img-inonav" src="img/logoino.png"></a>
                </li>
 				<li class="elemli-nav">
                    <a href="javascript:void(0);" class="lista-nav" data-idref="proyectos"><span>Proyectos</span></a>
                </li>
 				<li class="elemli-nav">
                    <a href="javascript:void(0);" class="lista-nav" data-idref="recursos"><span>Recursos</span></a>
                </li>
            </ul>
		</div>
	</div>
</nav>
    
<div class="holamundo-wrap">
    <div class="holamundo-inn">
        <img class="holamundo-img" src="img/imagenino.jpg">
        <div class="holamundo-txt"><img src="img/(hola-mundo).png" alt="Saludo al mundo" class="hola-mundo-img"></div>
        <div class="head-descr-wrap">
            <div class="head-descr-1">
                En Inoloop
            </div>
            <div class="head-descr-2">
                desarrollamos proyectos integrales
            </div>
            <div class="head-descr-3">
                para tu empresa
            </div>
        </div>
    </div>
</div>
    
<div class="container cont-body cont-ind">
    <div class="row">
        <div class="text-gener-wrapper" id="servicios">
            <div class="text-gener-inner">
                <div class="text-gener-tit">
                    <span class="sombr-span"> </span>
                    <span>Servicios</span>
                    
                </div>
                <div class="text-gener-text row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="lista-serv-wrap">
                            <div class="lista-serv-inner">
                                <span class="bola-listaserv" id="bola-1"> </span><span class="listaserv-active" id="listaserv-1" onclick="seleclistaserv(1)">Páginas web 2.0</span><br>
                                <span class="bola-listaserv bl-hide" id="bola-2"> </span><span class="listaserv-a" id="listaserv-2" onclick="seleclistaserv(2)">Aplicaciones Android</span><br>
                                <span class="bola-listaserv bl-hide" id="bola-3"> </span><span class="listaserv-a" id="listaserv-3" onclick="seleclistaserv(3)">Desarrollo de software</span><br>
                                <span class="bola-listaserv bl-hide" id="bola-4"> </span><span class="listaserv-a" id="listaserv-4" onclick="seleclistaserv(4)">Electrónica</span><br>
                                <span class="bola-listaserv bl-hide" id="bola-5"> </span><span class="listaserv-a" id="listaserv-5" onclick="seleclistaserv(5)">Internet de las cosas</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="descr-serv-wrapp">
                             <div class="descr-serv-inn">
                                 <div class="descr-serv-anim descr-serv-active" id="descr-serv-1">
                                     <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                      <!-- Indicators -->


                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        <div class="item active personalizada-cr">
                                            <div class="descr-tit"><div>Personalizada</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Al programar la página web desde cero le podemos garantizar una página web <b>única</b>, acorde a la imagen y medida de su empresa. 
                                                </div>

                                             </div>
                                        </div>

                                        <div class="item administrable-cr">
                                            <div class="descr-tit"><div>Administrable</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Se podrá actualizar el contenido de la página web, tanto textos como imágenes, creando así una página web <b>dinámica</b> y más interesante para sus usuarios.
                                                </div>
                                                
                                             </div>
                                        </div>

                                        <div class="item responsive-cr">
                                            <div class="descr-tit"><div>Responsiva</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    La página web tendrá un diseño <b>adaptable</b> al tamaño del dispositivo desde el que se la ve, para que así el usuario tenga una mejor experiencia al acceder a la página. 
                                                </div>

                                             </div>
                                        </div>
                                      </div>

                                      <!-- Left and right controls -->
                                      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </div>

                                 </div>

                                 <div class="descr-serv-anim descr-serv" id="descr-serv-2">
                                     <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                                      <!-- Indicators -->


                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        <div class="item active unica-cr">
                                            <div class="descr-tit"><div>Original</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Tus necesidades se verán satisfechas en una aplicación única y <b>original</b> que será desarrollada con las mejores metodologías y herramientas.
                                                </div>

                                             </div>
                                        </div>

                                        <div class="item soluciones-cr">
                                            <div class="descr-tit"><div>Soluciones</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Desarrollamos <b>aplicaciones</b> de negocios, juegos, redes sociales, seguridad, comercio electrónico, multimedia, viajes y muchas más.
                                                </div>
                                                
                                             </div>
                                        </div>
                                      </div>

                                      <!-- Left and right controls -->
                                      <a class="left carousel-control" href="#myCarousel2" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#myCarousel2" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </div>

                                 </div>
                                 
                                 <div class="descr-serv-anim descr-serv" id="descr-serv-3">
                                     <div id="myCarousel3" class="carousel slide" data-ride="carousel">
                                      <!-- Indicators -->


                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        <div class="item active eficiencia-cr">
                                            <div class="descr-tit"><div>Eficiencia</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Desarrollamos el software que tu negocio necesita, apoyándonos de los recursos <b>resuelvan</b> de manera más eficiente tus necesidades.
                                                </div>

                                             </div>
                                        </div>

                                        <div class="item calidad-cr">
                                            <div class="descr-tit"><div>Calidad</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    El software desarrollado cumplirá con tus expectativas, realizando sus funciones de manera <b>satisfactoria</b> y conforme a lo requerido.
                                                </div>
                                                
                                             </div>
                                        </div>
                                      </div>

                                      <!-- Left and right controls -->
                                      <a class="left carousel-control" href="#myCarousel3" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#myCarousel3" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </div>
                                 </div>
                                 
                                 <div class="descr-serv-anim descr-serv" id="descr-serv-4">
                                     <div id="myCarousel4" class="carousel slide" data-ride="carousel">
                                      <!-- Indicators -->


                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        <div class="item active control-cr">
                                            <div class="descr-tit"><div>Automatización y control</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Creamos soluciones que volverán más <b>eficientes</b> los sistemas y procesos de tu empresa.
                                                </div>

                                             </div>
                                        </div>

                                        <div class="item reconocimiento-cr">
                                            <div class="descr-tit"><div>Reconocimiento y registro</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Desarrollamos <b>proyectos</b> de control de asistencia, registro de producto, registro de inventario, entre otros. 
                                                </div>
                                                
                                             </div>
                                        </div>
                                      </div>

                                      <!-- Left and right controls -->
                                      <a class="left carousel-control" href="#myCarousel4" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#myCarousel4" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </div>

                                 </div>
                                 
                                 <div class="descr-serv-anim descr-serv" id="descr-serv-5">
                                     <div id="myCarousel5" class="carousel slide" data-ride="carousel">
                                      <!-- Indicators -->


                                      <!-- Wrapper for slides -->
                                      <div class="carousel-inner">
                                        <div class="item active comunicacion-cr">
                                            <div class="descr-tit"><div>Conexión</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Le damos la capacidad de <b>comunicarse</b> al objeto que desees.
                                                </div>

                                             </div>
                                        </div>

                                        <div class="item softwarenext-cr">
                                            <div class="descr-tit"><div>Software++</div></div>
                                            <div class="descr-descr">
                                                <div class="descr-descr-inn">
                                                    Llevamos tu <b>página web/ app/ software</b> al siguiente nivel al integrarlo con el mundo exterior.
                                                </div>
                                                
                                             </div>
                                        </div>
                                      </div>

                                      <!-- Left and right controls -->
                                      <a class="left carousel-control" href="#myCarousel5" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                        <span class="sr-only">Previous</span>
                                      </a>
                                      <a class="right carousel-control" href="#myCarousel5" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                        <span class="sr-only">Next</span>
                                      </a>
                                    </div>

                                 </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="text-gener-wrapper" id="cotiza">
            <div class="text-gener-inner">
                <div class="text-gener-tit">
                    <span class="sombr-span"> </span>
                    <span>Cotiza</span>
                </div>
                <div class="text-gener-text row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="cotiza-txt-wrap">
                            <div class="cotiza-txt-inn">
                                <div class="cotiza-txt-princ">Solicita tu cotización hoy sin compromiso</div>
                                <div class="cotiza-txt-sec">Estaremos entusiasmados de atenderte</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="cotiza-form-wrap">
                            <div class="cotiza-form-inn">
                                <form class="form-cotiza" method="post">
                                    <div class="flecha-ind-wrapp"><img src="img/flechaino.png" class="flecha-ind" alt="flecha indicadora"></div>
                                    <div class="form-group" id="nombre-form-wrap">
                                      <input type="text" class="form-control form-control-norm" id="nombre-cotiza" name="nombre-cotiza" placeholder="Introduzca su nombre" oninput="verificarnombre()">
                                    </div>
                                    <div class="form-group" id="email-form-wrap">
                                        <input type="email" class="form-control form-control-norm" id="email-cotiza" name="email-cotiza" placeholder="Introduzca su email" oninput="verificarcorreo()">
                                    </div>
                                    <div class="form-group" id="numero-form-wrap">
                                        <input type="number" class="form-control form-control-norm" id="numero-cotiza" name="numero-cotiza" placeholder="Introduzca su número" oninput="verificarnumero()">
                                    </div>
                                    <div class="form-group" id="texto-form-wrap">
                                        <textarea class="form-control form-control-norm" rows="5" placeholder="Descríbenos brevemente qué es lo que buscas." id="texto-cotiza" name="texto-cotiza" oninput="verificartexto()"></textarea>
                                    </div>
                                    <div class="btn-enviar-wrap">
                                        <button type="submit" class="btn btn-default btn-enviar-datos" name="enviar-info" id="enviar-info"><img src="img/flechaenviar.png" class="enviar-flecha" alt="flecha">Enviar</button>
                                    </div>
                                    
                                </form>

                            </div>           
                        
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="text-gener-wrapper" id="proyectos">
            <div class="text-gener-inner">
                <div class="text-gener-tit">
                    <span class="sombr-span"> </span>
                    <span>Proyectos</span>
                </div>
                <div class="text-gener-text row">
                    <div class="col-xs-12">
                        <div class="proyecto-form-wrap">
                            <div class="proyecto-form-inn">
                                <div class="tarjeta-proy-wrapp">
                                    <div class="tarjeta-proy-inn">
                                         <div id="myCarouselproy1" class="carousel slide" data-ride="carousel">
                                          <!-- Indicators -->


                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner">
                                            <div class="item active">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/eucl-1.png">
                                            </div>

                                            <div class="item">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/eucl-2.png">
                                            </div>

                                            <div class="item">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/eucl-3.png">
                                            </div>
                                          </div>

                                        </div>
                                        
                                    </div>
                                    <div class="tarjeta-proy-descr"><a href="https://www.colegioeuclides.edu.pe//" target="_blank" class="link-proy">Colegio Euclides</a></div>
                                </div>
                                
                                <div class="tarjeta-proy-wrapp">
                                    <div class="tarjeta-proy-inn">
                                         <div id="myCarouselproy2" class="carousel slide" data-ride="carousel">
                                          <!-- Indicators -->


                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner">
                                            <div class="item active">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/acuerdo-1.png">
                                            </div>

                                            <div class="item">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/acuerdo-2.png">
                                            </div>

                                            <div class="item">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/acuerdo-3.png">
                                            </div>
                                          </div>

                                        </div>
                                        
                                    </div>
                                    <div class="tarjeta-proy-descr"><a href="http://www.acuerdoregional.com/" target="_blank" class="link-proy">Acuerdo Regional</a></div>
                                </div>
                                
                                
                                <div class="tarjeta-proy-wrapp">
                                    <div class="tarjeta-proy-inn">
                                         <div id="myCarouselproy3" class="carousel slide" data-ride="carousel">
                                          <!-- Indicators -->


                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner">
                                            <div class="item active">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/inoloop-1.png">
                                            </div>

                                            <div class="item">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/inoloop-2.png">
                                            </div>

                                            <div class="item">
                                                <img class="tarjeta-proy-img" alt="Imagen de proyecto" src="img/inoloop-3.png">
                                            </div>
                                          </div>

                                        </div>
                                        
                                    </div>
                                    <div class="tarjeta-proy-descr"><a href="http://www.inoloop.com/" target="_blank" class="link-proy">Inoloop</a></div>
                                </div>
                                
                                
                                
                                
                            </div>           
                        
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <div class="text-gener-wrapper" id="recursos">
            <div class="text-gener-inner">
                <div class="text-gener-tit">
                    <span class="sombr-span"> </span>
                    <span>Recursos</span>
                </div>
                <div class="text-gener-text row row-recursos">
                    <div class="col-xs-12">
                        <div id="carrec-wrapper">
                            <div class="list_carousel">
                                <ul id="foo0">
                                    <li>
                                        <div class="recurso-wrapp">
                                            <div class="recurso-inn">
                                                <img class="recurso-img" alt="imagen de recurso" src="img/html5.png">
                                            </div>
                                            <div class="recurso-descr">
                                                HTML5
                                            </div>
                                        </div>

                                    </li>
                                    <li>
                                    
                                        <div class="recurso-wrapp">
                                            <div class="recurso-inn">
                                                <img class="recurso-img" alt="imagen de recurso" src="img/CSS3.png">
                                            </div>
                                            <div class="recurso-descr">
                                                CSS3
                                            </div>
                                        </div>
                                
                                    </li>
                                    <li>
                                    
                                    
                                        <div class="recurso-wrapp">
                                            <div class="recurso-inn">
                                                <img class="recurso-img" alt="imagen de recurso" src="img/javascript.png">
                                            </div>
                                            <div class="recurso-descr">
                                                Javascript
                                            </div>
                                        </div>

                                    
                                    </li>
                                    <li>
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/Ajax1.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Ajax
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/msql1.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Mysql
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/php-rombo.png">
                                    </div>
                                    <div class="recurso-descr">
                                        PHP
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/nodejs.png">
                                    </div>
                                    <div class="recurso-descr">
                                        NodeJS
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/linux.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Linux
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/Java_avatar.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Java
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/Android_Studio_icon.svg.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Android Studio
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/c-lang.png">
                                    </div>
                                    <div class="recurso-descr">
                                        C
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/c++.png">
                                    </div>
                                    <div class="recurso-descr">
                                        C++
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                    
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/python-logo.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Python
                                    </div>
                                </div>

                                    
                                    </li>
                                    <li>
                                        
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/Raspberry_Pi.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Raspberry Pi
                                    </div>
                                </div>

                                        
                                    </li>
                                    <li>
                                        
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/arduino-icon-1.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Arduino
                                    </div>
                                </div>

                                        
                                    </li>
                                    <li>
                                        
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/matlab.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Matlab
                                    </div>
                                </div>

                                        
                                    </li>
                                    <li>
                                        
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/visualstudio.png">
                                    </div>
                                    <div class="recurso-descr">
                                        Visual Studio
                                    </div>
                                </div>
                                        
                                    </li>
                                    <li>
                                        
                                <div class="recurso-wrapp">
                                    <div class="recurso-inn">
                                        <img class="recurso-img" alt="imagen de recurso" src="img/KNX-domotica.png">
                                    </div>
                                    <div class="recurso-descr">
                                        KNX
                                    </div>
                                </div>
                                        
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
            
        </div>

     </div>
</div>


<footer class="foot-wrap-in">
    

        <div class="izq-foot-inn"> 
            <div class="cir-grand"> </div>
            <div class="logo-foot-wrapp"><img class="logo-foot" src="img/logo-ino.png"> </div>

        </div>

    
    <div class="der-foot-wrap">
        <div class="der-foot-inn">
            <div class="der-foot-tit">
                Te dejamos nuestro contacto
            </div>
            <div class="der-foot-num">
                <a class="foot-icon" id="whatsapp-wrapp" href="intent://send/+51961518524#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end"><img src="img/whatsapp.png" alt="Logo de whatsapp"></a><a class="foot-icon" href="tel:+51961518524"><img src="img/llamada.png" alt="Logo de llamada"></a><span id="numero-telf">(+511) 961518524</span>
            </div>
            <div class="der-foot-num">
                <a class="foot-icon" href="mailto:inoloopsac@gmail.com" target="_top"><img src="img/correo2.png" alt="Logo de llamada" ></a><span id="corre-span">inoloopsac@gmail.com</span>
            </div>
            <div class="der-foot-num">
                <a class="foot-icon" href="https://www.facebook.com/inoloop" target="_blank"><img src="img/fb-art.png" alt="Logo de llamada"></a><span id="fb-span">fb/Inoloop</span>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.raty.js"></script>
<script src="js/princ.js"></script>
<script src="js/jquery.carouFredSel.js"></script>
<script>

//	Basic carousel, no options
//	Basic carousel, no options

$('#foo0').carouFredSel({
    
         width: '200px',
    responsive: true,
    scroll: {
        items: 1,
        duration: 2000,
        timeoutDuration: 0,
        easing: 'linear',
        pauseOnHover: 'immediate'
    },
    items: {
         width: '200',
    //	height: '30%',	//	optionally resize item-height
        visible: {
            min: 1,
            max:10
        }
    }
});

$( document ).ready(function() {

    document.getElementById("whatsapp-wrapp").style.visibility = "hidden";
    var ua = navigator.userAgent.toLowerCase();
    var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
    if(isAndroid) {

    document.getElementById("whatsapp-wrapp").style.visibility = "visible";



    }
});
</script>
<?php 

if (isset($_POST['enviar-info'])){
	if(!empty($_POST['nombre-cotiza']) && !empty($_POST['email-cotiza']) && !empty($_POST['numero-cotiza']) && !empty($_POST['texto-cotiza'])){
		$nombre_solicitante=$_POST['nombre-cotiza'];
		$correo_solicitante=$_POST['email-cotiza'];
		$numero_solicitante=$_POST['numero-cotiza'];
		$descripcion=$_POST['texto-cotiza'];

		enviarconfirmacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        enviarcotizacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        
        echo "<script>alert('Gracias por pensar en nosotros. Su cotización ha sido enviada.')</script>";


	}
}

?>
</body>
</html>
