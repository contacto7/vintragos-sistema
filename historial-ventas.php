<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

$cant_list = 20;
$temp_pag = 0;

if(isset($_GET['xid_last_id'])){
    $last_id_ventas = $_GET['xid_last_id'];
    $temp_pag = $_GET['pag_num'];
    
}else{
    $last_id_ventaslist = ultimoidventa();
    while ($row=mysqli_fetch_assoc($last_id_ventaslist)) {

        $last_id_ventas =$row['id_venta'];
    }
}

$listar_ventas_us = listarventasusuario($id_usuario, $last_id_ventas, $cant_list);
$head_ret_list = "";    
$head_adi_list = "";    

$monto_total_lista = 0;

$objetosventa = array();

while ($row=mysqli_fetch_assoc($listar_ventas_us)) {
    
    $id_venta_list =$row['id_venta'];
    $fec_venta_list =$row['fec_venta'];
    $descuento_venta =$row['descuento_venta'];
    $retencion_envase_venta_list =$row['retencion_envase_venta'];
    $adicional_venta_list =$row['adicional_venta'];
    $cod_interno_list =$row['cod_interno'];
    $cantidad_venta_list =$row['cantidad_venta'];
    $subproducto_venta_list =$row['subproducto_venta'];
    $monto_venta_list =$row['monto_venta'];
    
    if($retencion_envase_venta_list > 0){
        $head_ret_list = "Sí";
    }else{
        $head_ret_list = "No";
    }
    if($adicional_venta_list){
        $head_adi_list = "Sí";
    }else{
        $head_adi_list = "No";
    }
    
    $fec_venta_list = date_create($fec_venta_list);
    $fec_venta_list = date_format($fec_venta_list,"d/m/Y H:i:s");
    
    if($subproducto_venta_list){
        $cod_interno_list =$row['cod_interno_subproducto'];
    }
    
    if($cantidad_venta_list){
        $submonto_venta_list =$monto_venta_list / $cantidad_venta_list;
    }else{
        $cod_interno_list.=" (subproductos)";
        $submonto_venta_list =$monto_venta_list;
    }
     

    $tmp = new stdClass;
    
    $tmp->id_venta_list = $id_venta_list;
    $tmp->fec_venta_list = $fec_venta_list;
    $tmp->head_ret_list = $head_ret_list;
    $tmp->descuento_venta = $descuento_venta;
    $tmp->retencion_envase_venta_list = $retencion_envase_venta_list;
    $tmp->adicional_venta_list = $adicional_venta_list;
    $tmp->cod_interno_list = $cod_interno_list;
    $tmp->cantidad_venta_list = $cantidad_venta_list;
    $tmp->subproducto_venta_list = $subproducto_venta_list;
    $tmp->monto_venta_list = $monto_venta_list;
    $tmp->head_ret_list = $head_ret_list;
    $tmp->head_adi_list = $head_adi_list;
    $tmp->submonto_venta_list = $submonto_venta_list;

    $objetosventa[$id_venta_list][]=$tmp;
    //rray_push($objetosventa[$id_venta_list],$tmp);
    


}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    

    <div class="panel-group" id="accordion">
        <div class="panel panel-vintragos">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <div>
                        Fecha y hora | Monto total | Retención | Adicional  
                    </div>
                </h4>
            </div>
        </div>
<?php 
$cantidad_ventas_listadas = 0;
foreach ($objetosventa as $objetoventa) {
    
    $cantidad_ventas_listadas++;
    $monto_total = 0;
    
?>
        
        <div class="panel panel-vintragos">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $objetoventa[0]->id_venta_list; ?>">
                        <?php echo $objetoventa[0]->fec_venta_list; ?> |
                        S/.<span id="monto-headtot-<?php echo $objetoventa[0]->id_venta_list; ?>"><?php echo $objetoventa[0]->monto_venta_list; ?></span> |
                        <span id="retencion-head-<?php echo $objetoventa[0]->id_venta_list; ?>"><?php echo $objetoventa[0]->head_ret_list; ?></span> |
                        <?php echo $objetoventa[0]->head_adi_list; ?>  
                    </a>
                </h4>
            </div>
            <div id="collapse-<?php echo $objetoventa[0]->id_venta_list; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                    
                    <div class="info-venta-wrapp">
                        <div class="info-venta-inn">
                              <table class="table table-striped table-bordered table-venta">
                                <thead>
                                  <tr>
                                    <th class="th-venta-cod">Código</th>
                                    <th class="th-venta-cant">Cant.</th>
                                    <th class="th-venta-punit">P. unit</th>
                                    <th class="th-venta-ptot">P. tot</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    
<?php 
    foreach ($objetoventa as $objetosubventa) {
        $monto_total+=$objetosubventa->monto_venta_list;
?> 
                                    
                                  <tr class="tr-venta-detalle">
                                      <td class="td-venta-cod">
                                          <?php echo $objetosubventa->cod_interno_list; ?>
                                      </td>
                                      <td class="td-venta-cant">
                                          <?php echo $objetosubventa->cantidad_venta_list; ?>
                                      </td>
                                      <td class="td-venta-punit">
                                          <?php echo number_format($objetosubventa->submonto_venta_list, 2, '.', ' '); ?>
                                      </td>
                                      <td class="td-venta-ptot">
                                          <?php echo $objetosubventa->monto_venta_list; ?>
                                      </td>
                                  </tr>
                                    
<?php 
  }
?>                     
<?php 
  if( $objetoventa[0]->descuento_venta > 0){
      
      $monto_total-=$objetoventa[0]->descuento_venta;
?>   
                                    
                                  <tr class="tr-venta-monto">
                                    <td class="td-venta-vacio" colspan="2">                   
                                    </td>
                                    <td class="td-venta-total">Descuento</td>
                                    <td class="td-venta-montotot monto-total-tbl" id="td-venta-montotot" data-id="<?php echo $objetoventa[0]->id_venta_list; ?>">
                                        <?php echo $objetoventa[0]->descuento_venta; ?>
                                      </td>
                                  </tr>

                   
<?php 
  }
?>    
                                  <tr class="tr-venta-monto">
                                    <td class="td-venta-vacio" colspan="2">
<?php 
  if( $objetoventa[0]->retencion_envase_venta_list > 0){
?>                       
                                        <button class="btn btn-info btn-dev-ret" id="btn-devolv-<?php echo $objetoventa[0]->id_venta_list; ?>" data-id="<?php echo $objetoventa[0]->id_venta_list; ?>">Devolver retención</button>
<?php 
  }
?>                       
                                    </td>
                                    <td class="td-venta-total">Total</td>
                                    <td class="td-venta-montotot monto-total-tbl" id="td-venta-montotot" data-id="<?php echo $objetoventa[0]->id_venta_list; ?>">
                                        <?php echo number_format($monto_total, 2, '.', ' '); ?>
                                      </td>
                                  </tr>
                                </tbody>
                              </table>
                        </div>
                    </div>
                    <div class="infoad-venta-wrapp">
                        <div class="infoad-venta-inn form-inline">
                            <div class="form-group retencion-wrapp">
                                <label for="retencion">Retención:</label>
                                S/.<span id="retencion-<?php echo $objetoventa[0]->id_venta_list; ?>"><?php echo $objetoventa[0]->retencion_envase_venta_list; ?></span>
                            </div>                  
                            <div class="form-group adicional-wrapp">
                                <label for="adicional">Adicional:</label>
                                <span><?php echo $objetoventa[0]->adicional_venta_list; ?></span>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
<?php 

}
        
?>
                                    
    </div>
         
    <div class="mov-list-historial">
<?php if($temp_pag > 0){ ?>
        <a href="historial-ventas.php?xid_last_id=<?php echo $last_id_ventas + $cant_list; ?>&pag_num=<?php echo $temp_pag -1; ?>" class="btn btn-info">Anterior</a>
<?php } ?>
<?php if($cantidad_ventas_listadas == $cant_list){ ?>
        <a href="historial-ventas.php?xid_last_id=<?php echo $last_id_ventas - $cant_list; ?>&pag_num=<?php echo $temp_pag +1; ?>" class="btn btn-info">Siguiente</a>
<?php } ?>
    </div>

</div>
    

<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 2,
       xhs: 0
   });
    
    
    $( ".monto-total-tbl" ).each(function( index ) {
        var data_id = $(this).attr("data-id");
        var monto_tot = $(this).html();
        $("#monto-headtot-"+data_id).html(monto_tot);
    });
});


$(document).on("click", '.btn-dev-ret',function(){
    var id_venta = $(this).attr("data-id");
    $.ajax({
        type: 'post', 
        data: { 
            xid_venta: id_venta 
        }, 
        url: 'apost_eliminar_retventa.php',
        success: function (result) {
            if(result == 1){
                alert("Ocurrió un error")
            }else{
                $("#btn-devolv-"+id_venta).remove();
                $("#retencion-head-"+id_venta).html("No");
                $("#retencion-"+id_venta).html("0.00");
            }
            
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
    });
});
    
</script>

    
<?php 

if (isset($_POST['enviar-info'])){
	if(!empty($_POST['nombre-cotiza']) && !empty($_POST['email-cotiza'])){
		$nombre_solicitante=$_POST['nombre-cotiza'];
		$correo_solicitante=$_POST['email-cotiza'];
		$numero_solicitante=$_POST['numero-cotiza'];
		$descripcion="inicio:".$_POST['texto-cotiza'];

		enviarconfirmacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        enviarcotizacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        
        echo "<script>alert('Gracias por pensar en nosotros. Su solicitud de cotización ha sido enviada.')</script>";


	}else{

        echo "<script>alert('Por favor complete los datos requeridos.')</script>";
    }
}

?>
</body>
</html>
