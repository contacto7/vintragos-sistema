<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
}else{
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    //echo "<script> window.location.href='index.php';</script>";
}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link rel="stylesheet" href="css/keyboard.css"> 
    <link rel="stylesheet" href="css/keyboard-dark.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">

            <form class="form-horizontal" method="post">
              <div class="form-group">
                <label class="control-label col-sm-2" for="correo-adm">Código:</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="cod-adm" name="cod-adm" placeholder="Ingrese su código">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="cont-adm">Contraseña:</label>
                <div class="col-sm-10"> 
                  <input type="password" class="form-control" id="cont-adm" name="cont-adm" placeholder="Ingrese su contraseña">
                </div>
              </div>
              <div class="form-group"> 
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-default" name="ingresa-adm">Ingresar</button>
                </div>
              </div>
            </form>
        
        </div>
    </div>
</div>
    

<div id="footer"></div>
    
<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/jquery.keyboard.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/jquery.keyboard.extension-typing.js"></script>
<script src="js/jquery.keyboard.extension-autocomplete.js"></script>
<script src="js/jquery.keyboard.extension-caret.js"></script>
<script src="js/princ.js"></script>
<script src="js/touch.js"></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
$(function(){
   $("#header").load("header.php", {
       xph: 0,
       xhs: 0
   });
    
});
</script>
    
    
<?php 
    
if (isset($_POST['ingresa-adm'])){

    if(!empty($_POST['cod-adm']) || !empty($_POST['cont-adm']) ){

        $codigo_adm=$_POST['cod-adm'];
        $contrasena_adm=$_POST['cont-adm'];
        
        $pagdireccion = "registro-ventas.php";

        ingresa($codigo_adm,$contrasena_adm,$pagdireccion);

    }

}

?>
</body>
</html>
