<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    if (($_SESSION['poder_usuario']) <= 2) {
        echo "<script> window.location.href='registro-ventas.php';</script>";
    }
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

$ano_estado = 0;
$mes_estado = 0;
$tienda_estado = 0;

if(isset($_GET['xano']) && isset($_GET['xmes']) && isset($_GET['xtienda'])){
    $ano_estado = $_GET['xano'];
    $mes_estado = $_GET['xmes'];
    $tienda_estado = $_GET['xtienda'];
    
}else{
    echo "<script> window.location.href='historial-ganancias-perdidas.php';</script>";
}

$cuenta = 0;
$objetosotrosingresos = array();
$estado_ganacias_ex = verificarestadoganaciasperdidas($ano_estado,$mes_estado,$tienda_estado);
while ($row=mysqli_fetch_assoc($estado_ganacias_ex)) {
    $cuenta++;
    $id_reporte_mensual =$row['id_reporte_mensual'];
    $id_tienda =$row['id_tienda'];
    $fecha_reporte_mensual =$row['fecha_reporte_mensual'];
    $ventas_reporte_mensual =$row['ventas_reporte_mensual'];
    $compras_reporte_mensual =$row['compras_reporte_mensual'];
    $descuentos_reporte_mensual =$row['descuentos_reporte_mensual'];
    $alquiler_reporte_mensual =$row['alquiler_reporte_mensual'];
    $seguro_reporte_mensual =$row['seguro_reporte_mensual'];
    $articulosof_reporte_mensual =$row['articulosof_reporte_mensual'];
    $servpublicos_reporte_mensual =$row['servpublicos_reporte_mensual'];
    $impuestos_reporte_mensual =$row['impuestos_reporte_mensual'];
    
    //OTROS GASTOS E INGRESOS
    $id_otrosgasting =$row['id_otrosgasting'];
    $id_reporte_mensual =$row['id_reporte_mensual'];
    $tipo_otrosgasting =$row['tipo_otrosgasting'];
    $monto_otrosgasting =$row['monto_otrosgasting'];
    $descr_otrosgasting =$row['descr_otrosgasting'];
    
    
    $tmp = new stdClass;
    
    $tmp->id_otrosgasting = $id_otrosgasting;
    $tmp->id_reporte_mensual = $id_reporte_mensual;
    $tmp->tipo_otrosgasting = $tipo_otrosgasting;
    $tmp->monto_otrosgasting = $monto_otrosgasting;
    $tmp->descr_otrosgasting = $descr_otrosgasting;

    $objetosotrosingresos[]=$tmp;

}
if($cuenta == 0){
    $estado_ganacias_ins = insertarestadoganaciasperdidas($ano_estado,$mes_estado,$tienda_estado);
    if($estado_ganacias_ins){
        echo "<script> window.location.href='historial-gp.php?xano=$ano_estado&xmes=$mes_estado&xtienda=$tienda_estado';</script>";
    }else{
        echo "<script>alert('Ocurrió un error al generar el estado de ganancias y pérdidas. Intente entrando de nuevo.');</script>";
        echo "<script> window.location.href='historial-ganancias-perdidas.php';</script>";
    }
}

$otros_ingresos = 0;

foreach ($objetosotrosingresos as $objetootrosingresos) {
    $id_otrosgasting = $objetootrosingresos->id_otrosgasting;
    $id_reporte_mensual = $objetootrosingresos->id_reporte_mensual;
    $tipo_otrosgasting = $objetootrosingresos->tipo_otrosgasting;
    $monto_otrosgasting = $objetootrosingresos->monto_otrosgasting;
    $descr_otrosgasting = $objetootrosingresos->descr_otrosgasting;
    
    if($tipo_otrosgasting == 1){
        $otros_ingresos+=$monto_otrosgasting;
    }else{
        $otros_ingresos-=$monto_otrosgasting;
    }
    
}
    
//Valores para operar
$ganancia_bruta = $ventas_reporte_mensual - $compras_reporte_mensual - $descuentos_reporte_mensual;

$gastos_fijos = $alquiler_reporte_mensual + $seguro_reporte_mensual + $articulosof_reporte_mensual + $servpublicos_reporte_mensual;

$ingresos_operativos = $ganancia_bruta - $gastos_fijos;

$ganancia_antes_imp = $ingresos_operativos + $otros_ingresos;

$ganancia_neta = $ganancia_antes_imp - $impuestos_reporte_mensual;

//FORMATO DE NÚMEROS

$alquiler_reporte_mensual_form = number_format($alquiler_reporte_mensual, 2, '.', ',');
$ventas_reporte_mensual_form = number_format($ventas_reporte_mensual, 2, '.', ',');
$compras_reporte_mensual_form = number_format($compras_reporte_mensual, 2, '.', ',');
$descuentos_reporte_mensual_form = number_format($descuentos_reporte_mensual, 2, '.', ',');
$seguro_reporte_mensual_form = number_format($seguro_reporte_mensual, 2, '.', ',');
$articulosof_reporte_mensual_form = number_format($articulosof_reporte_mensual, 2, '.', ',');
$servpublicos_reporte_mensual_form = number_format($servpublicos_reporte_mensual, 2, '.', ',');
$impuestos_reporte_mensual_form = number_format($impuestos_reporte_mensual, 2, '.', ',');

$ganancia_bruta_form = number_format($ganancia_bruta, 2, '.', ',');
$gastos_fijos_form = number_format($gastos_fijos, 2, '.', ',');
$ingresos_operativos_form = number_format($ingresos_operativos, 2, '.', ',');
$otros_ingresos_form = number_format($otros_ingresos, 2, '.', ',');
$ganancia_antes_imp_form = number_format($ganancia_antes_imp, 2, '.', ',');
$ganancia_neta_form = number_format($ganancia_neta, 2, '.', ',');


?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="tabl-est-wrapp">
            
                <div class="tabl-est-inn">
                    <table class="table table-bordered table-responsive">
                        <thead>
                          <tr>
                            <th colspan="2"><b>ESTADO DE GANACIAS Y PÉRDIDAS</b></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td colspan="2"><b>Ingresos operativos</b></td>
                          </tr>
                          <tr>
                            <td>Ventas de productos</td>
                            <td>
                                <?php echo $ventas_reporte_mensual_form; ?> 
                                 <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="1" data-dscr="Ventas de productos" data-val="<?php echo $ventas_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Ingresos operativos totales</b></td>
                            <td>
                                <span id="a-2-td"><?php echo $ventas_reporte_mensual_form; ?></span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2"><b>Gastos operativos</b></td>
                          </tr>
                          <tr>
                            <td>Costo de mercancías vendidas</td>
                            <td>
                                -<?php echo $compras_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="2" data-dscr="Costo de mercancías vendidas" data-val="<?php echo $compras_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td>Descuentos</td>
                            <td>
                                -<?php echo $descuentos_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="8" data-dscr="Dscuentos" data-val="<?php echo $descuentos_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Ganancia bruta</b></td>
                            <td>
                                <span id="b-2-td"><?php echo $ganancia_bruta_form; ?></span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2"><b>Gastos fijos</b></td>
                          </tr>
                          <tr>
                            <td>Alquiler</td>
                            <td>
                                -<?php echo $alquiler_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="3" data-dscr="Alquiler" data-val="<?php echo $alquiler_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td>Seguro</td>
                            <td>
                                -<?php echo $seguro_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="4" data-dscr="Seguro" data-val="<?php echo $seguro_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td>Artículos de oficina</td>
                            <td>
                                -<?php echo $articulosof_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="5" data-dscr="Artículos de oficina" data-val="<?php echo $articulosof_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td>Servicios públicos</td>
                            <td id="f-td">
                                -<?php echo $servpublicos_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="6" data-dscr="Servicios públicos" data-val="<?php echo $servpublicos_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Total de gastos fijos</b></td>
                            <td>
                                -<span id="c-2-td"><?php echo $gastos_fijos_form; ?></span>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Ingresos operativos</b></td>
                            <td>
                                <span id="d-2-td"><?php echo $ingresos_operativos_form; ?></span>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="2"><b>Otros ingresos (gastos)</b></td>
                          </tr>
<?php 

foreach ($objetosotrosingresos as $objetootrosingresos) {
    $id_otrosgasting = $objetootrosingresos->id_otrosgasting;
    $id_reporte_mensual = $objetootrosingresos->id_reporte_mensual;
    $tipo_otrosgasting = $objetootrosingresos->tipo_otrosgasting;
    $monto_otrosgasting = $objetootrosingresos->monto_otrosgasting;
    $descr_otrosgasting = $objetootrosingresos->descr_otrosgasting;
    
    $monto_otrosgasting_form = number_format($monto_otrosgasting, 2, '.', ',');
    
    if($tipo_otrosgasting == 2){
        $monto_otrosgasting_form= "-".$monto_otrosgasting_form;
    }
    
    //Ciframos para saber que es otros gastos e ingresos
    $id_otrosgasting_cifrado = 100 + $id_otrosgasting;
?>
                          <tr>
                            <td><?php echo $descr_otrosgasting; ?></td>
                            <td>
                                <span id="ogi-<?php echo $id_otrosgasting; ?>"><?php echo $monto_otrosgasting_form; ?></span> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="<?php echo $id_otrosgasting_cifrado; ?>" data-dscr="<?php echo $descr_otrosgasting; ?>" data-val="<?php echo $monto_otrosgasting; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                <a class='remover-btn' href='#elim-row-mod' data-target='#elim-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="<?php echo $id_otrosgasting; ?>" data-dscr="<?php echo $descr_otrosgasting; ?>" data-val="<?php echo $monto_otrosgasting; ?>"><span class='glyphicon glyphicon-remove'></span></a>
                            </td>
                          </tr>
<?php 
    
}
    
                            
?>
                          <tr>
                            <td><input type="text" class="form-control" placeholder="Descripción de ingreso o gasto" id="nuev-gasting-dscr"></td>
                            <td>
                                <span>
                                    <input type="number" class="form-control" placeholder="Monto, si es gasto con - adelante" id="nuev-gasting-monto" step="0.01" style="display: inline-block;width: calc(100% - 64px);"> 
                                    <a class="btn btn-info btn-xs btn-agre-gasting">Agregar</a>
                                </span>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Ganancias antes de impuestos</b></td>
                            <td>
                                <span id="e-2-td"><?php echo $ganancia_antes_imp_form; ?></span>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Impuestos a la renta</b></td>
                            <td>
                                -<?php echo $impuestos_reporte_mensual_form; ?> 
                                <a class="btn btn-info btn-xs btn-mod-estcue" href='#edit-row-mod' data-target='#edit-row-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id="7" data-dscr="Impuestos a la renta" data-val="<?php echo $impuestos_reporte_mensual; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                            </td>
                          </tr>
                          <tr>
                            <td><b>Ganancias netas</b></td>
                            <td>
                                <span id="g-2-td"><?php echo $ganancia_neta_form; ?></span>
                            </td>
                          </tr>
                        </tbody>
                    </table>
                
                </div>
            
            </div>
        </div>
    </div>
</div>

<div id="edit-row-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Editar estado de ganancias y pérdidas</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                
                 <form class="form-horizontal form-registdel" role="form" action="" method="post"  enctype="multipart/form-data">
                    <div class="form-group row" style="display:none">
                      <label for="id-id" class="col-sm-4 col-form-label">Columna</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Id del inventario" name="id-row" id="id-row">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="val-row" class="col-sm-4 col-form-label"><span id="dscr-row"></span></label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Ingrese el valor" name="val-row" id="val-row" step="0.01">
                      </div>
                    </div>
                     <div class="form-group row">
                          <label for="" class="col-sm-4 col-form-label"></label>
                          <div class="col-sm-8">
                            <button type="submit" class="btn btn-sub-regdel btn-sm" name="camb-row-sub" onclick="$('#cargando-reg').show()">Guardar</button>
                          </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-reg" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div> 
    
    
<div id="elim-row-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar otro ingreso o gasto</h4>
            </div>
        
            <div class="modal-body vota-mod-bod">
                 <form class="form-horizontal form-registdel" role="form" action="" method="post"  enctype="multipart/form-data">
                     <div class="form-group row">
                         <label for="id-gasting" class="col-sm-12 col-form-label">¿Está seguro que desea eliminar: " <span id="otrgasting-dscrelim"></span>"?</label>
                     </div>
                    <div class="form-group row" style="display:none">
                      <label for="id-gasting" class="col-sm-4 col-form-label">Ingreso o gasto</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Id del ingreso o gasto" name="id-gasting" id="id-gasting">
                      </div>
                    </div>
                     <div class="form-group row">
                          <label for="" class="col-sm-4 col-form-label"></label>
                          <div class="col-sm-8">
                            <button type="submit" class="btn btn-sub-regdel btn-sm" name="elim-row-sub" onclick="$('#cargando-elim').show()">Eliminar</button>
                          </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-elim" style="display:none">Eliminando...</span>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div> 
    

<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_reporte_mensual = <?php echo $id_reporte_mensual; ?>;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 4,
       xhs: 0
   });
    
    
    $( ".monto-total-tbl" ).each(function( index ) {
        var data_id = $(this).attr("data-id");
        var monto_tot = $(this).html();
        $("#monto-headtot-"+data_id).html(monto_tot);
    });
    
    //Actualizar los valores

    
});


$(document).on("click", '.btn-agre-gasting',function(){
    var dscr_nuevingrgast = $("#nuev-gasting-dscr").val();
    var monto_nuevingrgast = $("#nuev-gasting-monto").val();
    
    if( (dscr_nuevingrgast != "") && (monto_nuevingrgast!= 0)){
            
        $.ajax({
            type: 'post', 
            data: { 
                xid_reporte_mensual: id_reporte_mensual,
                xdscr_nuevingrgast: dscr_nuevingrgast,
                xmonto_nuevingrgast: monto_nuevingrgast,
            }, 
            url: 'apost_agregar_gasting.php',
            success: function (result) {
                if(result == "Error-1"){
                    alert("Ocurrió un error. Verifique que los tipos de datos ingresados son correctos e intente de nuevo.")
                }else{
                    //console.log(result);
                    location.reload();
                }

            },
            error: function(xhr, status, error) {
              console.log(error);
                alert("Ocurrió un error. Verifique que los tipos de datos ingresados son correctos e intente de nuevo.")
            }
        });
    }else{
        
        alert("Debe ingresar ambos datos.");
       
   }

});
    
$(document).on("click", '.btn-mod-estcue',function(){
    
    var id_row=$(this).attr('data-id');
    var dscr_row=$(this).attr('data-dscr');
    var val_row=$(this).attr('data-val');
    
    $("#id-row").val(id_row);
    $("#dscr-row").html(dscr_row);
    $("#val-row").val(val_row);

});


$(document).on("click", '.remover-btn',function(){
    
    var id_row=$(this).attr('data-id');
    var dscr_row=$(this).attr('data-dscr');
    var val_row=$(this).attr('data-val');
    
    $("#id-gasting").val(id_row);
    $("#otrgasting-dscrelim").html(dscr_row);
});

</script>

    
<?php 

    
if(isset($_POST['camb-row-sub'])){

    if(!empty($_POST['id-row']) && !empty($_POST['val-row']) ){
        
        $id_row = $_POST['id-row'];
        $val_row = $_POST['val-row'];
        
        $mensaje_result = actualizarvalorestadogp($id_reporte_mensual, $id_row, $val_row);
        
        if(!$mensaje_result){
            echo $mensaje_result;
            echo "<script>alert('Hubo un error al actulizar el valor. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='historial-gp.php?xano=$ano_estado&xmes=$mes_estado&xtienda=$tienda_estado';</script>";
        }
        
    }else{
        echo "<script>alert('Debe ingresar los datos requeridos.');</script>";
        exit();
    }

}
    
if(isset($_POST['elim-row-sub'])){

    if(!empty($_POST['id-gasting']) ){
        
        $id_gasting_mod = $_POST['id-gasting'];
        
        $mensaje_result = eliminaringresoogasto($id_gasting_mod);
        
        if(!$mensaje_result){
            echo $mensaje_result;
            echo "<script>alert('Hubo un error al eliminar la fila. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script> window.location.href='historial-gp.php?xano=$ano_estado&xmes=$mes_estado&xtienda=$tienda_estado';</script>";
        }
        
    }else{
        echo "<script>alert('Los datos no se están llenando correctamente. Contáctese con servicio técnico.');</script>";
        exit();
    }

}

?>
</body>
</html>
