<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    if (($_SESSION['poder_usuario']) == 1) {
        echo "<script> window.location.href='registro-ventas.php';</script>";
    }
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

$msg="";

if(isset($_GET['xstate'])){
    
    if($_GET['xstate'] == 0){ 
        $msg = "<div class='msg-probl'><span class='glyphicon glyphicon-remove'></span> Ocurrió un error al subir los productos, revise que la tabla se haya llenado correctamente, y suba de nuevo el archivo.</div>";
    }elseif($_GET['xstate'] == 1){
        $msg = "<div class='msg-exitoso'><span class='glyphicon glyphicon-ok'></span> Se subió correctamente el archivo.</div>";
    }elseif($_GET['xstate'] == 2){
        $msg = "<div class='msg-probl'><span class='glyphicon glyphicon-remove'></span> Está intentando ingresar un producto con un código interno que ya existe en la base de datos.</div>";
    }
    
}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-info" href="#agre-inv-mod" data-target="#agre-inv-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">Agregar producto</button>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div><?php echo $msg; ?></div>
            <table class="table table-striped table-responsive table-grande">
                <thead>
                  <tr>
                    <th >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Id</th>
                    <th>Cod_interno</th>
                    <th>dscr</th>
                    <th>tipo</th>
                    <th>marca</th>
                    <th>med.</th>
                    <th>prec_com</th>
                    <th>prec_venta</th>
                    <th>desc.</th>
                    <th>lim.</th>
                    <th>prov.</th>
                    <th>Cant_sub</th>
                    <th>Subp_cod</th>
                  </tr>
                </thead>
                <tbody>
                    
<?php 
                    


$listar_producto = listarproducto();

while ($row=mysqli_fetch_assoc($listar_producto)) {
    
    $id_producto =$row['id_producto'];
    $cod_interno =$row['cod_interno'];
    $dscr_producto =$row['dscr_producto'];
    $id_tipo_producto =$row['id_tipo_producto'];
    $id_marca_producto =$row['id_marca_producto'];
    $id_medida_producto =$row['id_medida_producto'];
    $prec_compra_producto =$row['prec_compra_producto'];
    $prec_venta_producto =$row['prec_venta_producto'];
    $descuento_producto =$row['descuento_producto'];
    $limite_inventario_producto =$row['limite_inventario_producto'];
    $id_proveedor_producto =$row['id_proveedor_producto'];
    $cantidad_subproducto =$row['cantidad_subproducto'];
    $cod_interno_subproducto =$row['cod_interno_subproducto'];
    
                
    $btn_eliminar = "<a class='remover-btn' href='#elim-inv-mod' data-target='#elim-inv-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$id_producto' data-id='$id_producto' data-descr='$cod_interno'><span class='glyphicon glyphicon-remove'></span></a>";   
                
    $btn_editar = "<a class='editar-btn' href='#edit-inv-mod' data-target='#edit-inv-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id='$id_producto'><span class='glyphicon glyphicon-pencil'></span></a>";    
                    
?>
                  <tr id="<?php echo $id_producto; ?>">
                    <td id="0-<?php echo $id_producto; ?>"><?php echo $btn_eliminar; ?><?php echo $btn_editar; ?></td>
                    <td id="a-<?php echo $id_producto; ?>"><?php echo $id_producto; ?></td>
                    <td id="b-<?php echo $id_producto; ?>"><?php echo $cod_interno; ?></td>
                    <td id="c-<?php echo $id_producto; ?>"><?php echo $dscr_producto; ?></td>
                    <td id="d-<?php echo $id_producto; ?>"><?php echo $id_tipo_producto; ?></td>
                    <td id="e-<?php echo $id_producto; ?>"><?php echo $id_marca_producto; ?></td>
                    <td id="f-<?php echo $id_producto; ?>"><?php echo $id_medida_producto; ?></td>
                    <td id="g-<?php echo $id_producto; ?>"><?php echo $prec_compra_producto; ?></td>
                    <td id="h-<?php echo $id_producto; ?>"><?php echo $prec_venta_producto; ?></td>
                    <td id="i-<?php echo $id_producto; ?>"><?php echo $descuento_producto; ?></td>
                    <td id="j-<?php echo $id_producto; ?>"><?php echo $limite_inventario_producto; ?></td>
                    <td id="k-<?php echo $id_producto; ?>"><?php echo $id_proveedor_producto; ?></td>
                    <td id="l-<?php echo $id_producto; ?>"><?php echo $cantidad_subproducto; ?></td>
                    <td id="m-<?php echo $id_producto; ?>"><?php echo $cod_interno_subproducto; ?></td>
                  </tr>
<?php 
}    
                    
?>    
                    
                </tbody>
              </table>
        </div>
    </div>
</div>

    
<div id="agre-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar producto</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Descargue el siguiente documento, llénelo con los nuevos productos, selecciónelo y guárdelo.</label>
                            <div class="link-docu">
                                <a href="documentoej/producto.csv" dowload="">
                                    <span class="glyphicon glyphicon-circle-arrow-down"></span> PRODUCTOS
                                </a>
                            </div>
                        </div>
                     </div>
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Agregar productos</label>
                            <label for="file"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                            <div id="nombrefoto"></div>
                              <input type="file" class="archivo-subir" id="file" name="file" style="display:none" data-id="1">
                            <span class="span-foto-estado" id="estado-cambio-1"></span>
                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="importSubmit" onclick="$('#cargando-regpro').show()" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-regpro" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<div id="elim-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar inventario</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar el producto: " <span id="inv-dscr"></span>"?</div>
                <div class=""><b>IMPORTANTE: Si elimina el producto podría generar problemas.</b></div>
                <div class="mensaje-opc-elim"></div>
                <div class="inv-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elim-inv-btn" data-id="0" data-tr="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="edit-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Editar producto</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                
                 <form class="form-horizontal form-registdel" role="form" action="" method="post"  enctype="multipart/form-data">
                    <div class="form-group row" style="display:none">
                      <label for="id-id" class="col-sm-4 col-form-label">Id</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Id del inventario" name="id-inv" id="id-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="codint-inv" class="col-sm-4 col-form-label">Cod_interno</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Código interno" name="codint-inv" id="codint-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="dscr-inv" class="col-sm-4 col-form-label">Descr.</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Descripcion" name="dscr-inv" id="dscr-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="tipo-inv" class="col-sm-4 col-form-label">Tipo</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Tipo" name="tipo-inv" id="tipo-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="marca-inv" class="col-sm-4 col-form-label">Marca</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Marca" name="marca-inv" id="marca-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="med-inv" class="col-sm-4 col-form-label">Medida</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Medida" name="med-inv" id="med-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="preccom-inv" class="col-sm-4 col-form-label"> Precio_compra</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Precio de compra" name="preccom-inv" id="preccom-inv" step="0.01">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="precven-inv" class="col-sm-4 col-form-label">Precio_venta</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Precio de venta" name="precven-inv" id="precven-inv" step="0.01">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="descue-inv" class="col-sm-4 col-form-label">Descuento</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Descuento" name="descue-inv" id="descue-inv" step="0.01">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="lim-inv" class="col-sm-4 col-form-label">Limite</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Límite" name="lim-inv" id="lim-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="prov-inv" class="col-sm-4 col-form-label">Proveedor</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Proveedor" name="prov-inv" id="prov-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cantsub-inv" class="col-sm-4 col-form-label">Cantidad_subpro</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Proveedor" name="cantsub-inv" id="cantsub-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="codintsub-inv" class="col-sm-4 col-form-label">Cód. int. subpr.</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Código subproducto" name="codintsub-inv" id="codintsub-inv">
                      </div>
                    </div>
                     <div class="form-group row">
                          <label for="" class="col-sm-4 col-form-label"></label>
                          <div class="col-sm-8">
                            <button type="submit" class="btn btn-sub-regdel btn-sm" name="camb-inv-sub" onclick="$('#cargando-reg').show()">Guardar</button>
                          </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-reg" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div> 
    
<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 3,
       xhs: 0
   });
    
    
    $( ".monto-total-tbl" ).each(function( index ) {
        var data_id = $(this).attr("data-id");
        var monto_tot = $(this).html();
        $("#monto-headtot-"+data_id).html(monto_tot);
    });
});


$(document).on("click", '.btn-dev-ret',function(){
    var id_venta = $(this).attr("data-id");
    $.ajax({
        type: 'post', 
        data: { 
            xid_venta: id_venta 
        }, 
        url: 'apost_eliminar_retventa.php',
        success: function (result) {
            if(result == 1){
                alert("Ocurrió un error")
            }else{
                $("#btn-devolv-"+id_venta).remove();
                $("#retencion-head-"+id_venta).html("No");
                $("#retencion-"+id_venta).html("0.00");
            }
            
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
    });
});

$(document).on("click", '.remover-btn',function(){
    
    var id_opc=$(this).attr('data-id');
    var tr_id=$(this).attr('data-tr');
    var dscr_opc=$(this).attr('data-descr');
    
    
    $("#inv-dscr").html(dscr_opc);
    $(".elim-inv-btn").attr('data-id',id_opc);
    $(".elim-inv-btn").attr('data-tr',tr_id);

});

$(document).on("click", '.editar-btn',function(){
    
    var id_inv=$(this).attr('data-id');
    var codint_inv=$("#b-"+id_inv).html();
    var descr=$("#c-"+id_inv).html();
    var tipo=$("#d-"+id_inv).html();
    var marca=$("#e-"+id_inv).html();
    var medida=$("#f-"+id_inv).html();
    var prec_compr=$("#g-"+id_inv).html();
    var prec_venta=$("#h-"+id_inv).html();
    var descuento=$("#i-"+id_inv).html();
    var limite=$("#j-"+id_inv).html();
    var proveedor=$("#k-"+id_inv).html();
    var cant_subpro=$("#l-"+id_inv).html();
    var cond_int_subpro=$("#m-"+id_inv).html();
    
    
    $("#id-inv").val(id_inv);
    $("#codint-inv").val(codint_inv);
    $("#dscr-inv").val(descr);
    $("#tipo-inv").val(tipo);
    $("#marca-inv").val(marca);
    $("#med-inv").val(medida);
    $("#preccom-inv").val(prec_compr);
    $("#precven-inv").val(prec_venta);
    $("#descue-inv").val(descuento);
    $("#lim-inv").val(limite);
    $("#prov-inv").val(proveedor);
    $("#cantsub-inv").val(cant_subpro);
    $("#codintsub-inv").val(cond_int_subpro);

});

$(document).on("click", '.elim-inv-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    var i1_2 = $(this).attr('data-tr');
    //console.log(i1_1);
    //console.log(i1_2);
    
    $(".mensaje-opc-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_eliminar_producto.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elim-inv-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar el producto. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminó el producto con éxito.");
                $( "#"+i1_2).remove();
            }else if(result == 3){
                alert("El sistema no ha identificado el dominio como Vintragos. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-opc-elim").html("");
            
        }
    });


});
</script>

    
<?php 

    
if(isset($_POST['importSubmit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            $temp= 0;
            
            $cmdsql="INSERT INTO `producto`(`id_producto`, `cod_interno`, `dscr_producto`, `id_tipo_producto`, `id_marca_producto`, `id_medida_producto`, `prec_compra_producto`, `prec_venta_producto`, `descuento_producto`, `limite_inventario_producto`, `id_proveedor_producto`, `cantidad_subproducto`, `cod_interno_subproducto`) VALUES ";
            
            while(($line = fgetcsv($csvFile)) !== FALSE){
                $line[0] = utf8_encode($line[0]);
                $line[1] = utf8_encode($line[1]);
                $line[2] = utf8_encode($line[2]);
                $line[3] = utf8_encode($line[3]);
                $line[4] = utf8_encode($line[4]);
                $line[5] = utf8_encode($line[5]);
                $line[6] = utf8_encode($line[6]);
                $line[7] = utf8_encode($line[7]);
                $line[8] = utf8_encode($line[8]);
                $line[9] = utf8_encode($line[9]);
                $line[10] = utf8_encode($line[10]);
                $line[11] = utf8_encode($line[11]);
                
                if($temp == 0){
                    $cmdsql.= "(NULL, '".$line[0]."', '".$line[1]."', $line[2], $line[3], $line[4], $line[5], $line[6], $line[7], $line[8], $line[9], '".$line[10]."', '".$line[11]."' )";
                }else{
                    //insert member data into database
                    $cmdsql.= ", (NULL, '".$line[0]."', '".$line[1]."', $line[2], $line[3], $line[4], $line[5], $line[6], $line[7], $line[8], $line[9], '".$line[10]."', '".$line[11]."' )";
                }
                
                $temp++;
            }
            //echo $cmdsql;
            
            $link=conectar();
            //close opened csv file
            fclose($csvFile);
            
            $resultado=mysqli_query($link, $cmdsql);
            $mensaje= "";
            if(!$resultado){
                //echo "<br>Error de procedimiento";
                //echo mysqli_error($link);
                $error_nro = mysqli_errno($link);
                if($error_nro!=1062){
                    echo "<script> window.location.href='productos.php?xstate=0';</script>";
                }else{
                    echo "<script> window.location.href='productos.php?xstate=2';</script>";
                }
            }else{
                $estado_renovararch = renovararchivos();
                if($estado_renovararch=="Correcto"){
                    echo "<script> window.location.href='productos.php?xstate=1';</script>";
                    echo "<script>console.log('$primer_id_insertado')</script>";
                }else{
                    echo "<script> window.location.href='productos.php?xstate=3';</script>";
                }
            }

        }else{
            echo "<script>alert('Ocurrió algún error al subir el archivo, por favor intente de nuevo.')</script>";
        }
    }else{
        echo "<script>alert('Por favor suba un archivo CSV.')</script>";
    }
}

    
if(isset($_POST['camb-inv-sub'])){

    if(!empty($_POST['id-inv']) && !empty($_POST['codint-inv']) && !empty($_POST['dscr-inv']) && !empty($_POST['tipo-inv']) && !empty($_POST['marca-inv']) && !empty($_POST['med-inv']) && !empty($_POST['preccom-inv']) && !empty($_POST['precven-inv']) && !empty($_POST['prov-inv']) ){
        
        $id_inv = $_POST['id-inv'];
        $codint_inv = $_POST['codint-inv'];
        $dscr_inv = $_POST['dscr-inv'];
        $tipo_inv = $_POST['tipo-inv'];
        $marca_inv = $_POST['marca-inv'];
        $med_inv = $_POST['med-inv'];
        $preccom_inv = $_POST['preccom-inv'];
        $precven_inv = $_POST['precven-inv'];
        $descue_inv = $_POST['descue-inv'];
        $lim_inv = $_POST['lim-inv'];
        $prov_inv = $_POST['prov-inv'];
        $cantsub_inv = $_POST['cantsub-inv'];
        $codintsub_inv = $_POST['codintsub-inv'];
                
        $mensaje_result = actualizarproducto($id_inv, $codint_inv, $dscr_inv, $tipo_inv, $marca_inv, $med_inv, $preccom_inv, $precven_inv, $descue_inv, $lim_inv, $prov_inv, $cantsub_inv, $codintsub_inv);
        
        if(!$mensaje_result){
            echo $mensaje_result;
            echo "<script>alert('Hubo un error al actulizar el producto. Por favor intenta más tarde.')</script>";
            exit();
        }else{

            $estado_renovararch = renovararchivos();
            if($estado_renovararch=="Correcto"){
                echo "<script>alert('Se actualizó el producto.')</script>";
                echo "<script> window.location.href='productos.php';</script>";
            }else{
                echo "<script> window.location.href='productos.php?xstate=3';</script>";
            }
        }
        
    }else{
        echo "<script>alert('Debe ingresar los datos requeridos.');</script>";
        exit();
    }

}
    
?>
</body>
</html>
