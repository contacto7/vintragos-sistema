<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    if (($_SESSION['poder_usuario']) <= 2) {
        echo "<script> window.location.href='registro-ventas.php';</script>";
    }
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div action="">
                <div class="form-group row">
                  <label for="cargo-delegado" class="col-sm-2 col-form-label">Año</label>
                  <div class="col-sm-10">
                    <input class="form-control" type="number" placeholder="Formato XXXX. Ejemplo: 2018" name="ano-estado" id="ano-estado">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="cargo-delegado" class="col-sm-2 col-form-label">Mes</label>
                  <div class="col-sm-10">
                    <select class="form-control sel-unibydel" name="mes-estado" id="mes-estado">
                      <option class="sel-prov" value="1">Enero</option>
                      <option class="sel-prov" value="2">Febrero</option>
                      <option class="sel-prov" value="3">Marzo</option>
                      <option class="sel-prov" value="4">Abril</option>
                      <option class="sel-prov" value="5">Mayo</option>
                      <option class="sel-prov" value="6">Junio</option>
                      <option class="sel-prov" value="7">Julio</option>
                      <option class="sel-prov" value="8">Agosto</option>
                      <option class="sel-prov" value="9">Septiembre</option>
                      <option class="sel-prov" value="10">Octubre</option>
                      <option class="sel-prov" value="11">Noviembre</option>
                      <option class="sel-prov" value="12">Diciembre</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="tienda-estado" class="col-sm-2 col-form-label">Tienda</label>
                  <div class="col-sm-10">
                    <select class="form-control sel-unibydel" name="tienda-estado" id="tienda-estado">
<?php 
                    


$listar_inventario = listartiendas();

while ($row=mysqli_fetch_assoc($listar_inventario)) {
    
    $id_tienda =$row['id_tienda'];
    $nombre_tienda =$row['nombre_tienda'];
    $direccion_tienda =$row['direccion_tienda'];   
                    
?>
                      <option class="sel-prov" value="<?php echo $id_tienda; ?>"><?php echo $nombre_tienda; ?></option>
<?php 
}    
                    
?>    

                    </select>
                  </div>
                </div>
                <div class="mensaje-opc-agre"></div>
                <div class="vota-modbod-btn">
                    <button type="submit" class="btn btn-default btn-sm ver-votdel-btn" name="ver-votdel-btn" data-table="0" data-idagre="" onclick="verificarefecha($('#ano-estado').val() ,$('#mes-estado').val(), $('#tienda-estado').val())">Ver</button>                        
                </div>
            </div>
        </div>
    </div>
</div>
    

<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 4,
       xhs: 0
   });
    
    
    $( ".monto-total-tbl" ).each(function( index ) {
        var data_id = $(this).attr("data-id");
        var monto_tot = $(this).html();
        $("#monto-headtot-"+data_id).html(monto_tot);
    });
});


$(document).on("click", '.btn-dev-ret',function(){
    var id_venta = $(this).attr("data-id");
    $.ajax({
        type: 'post', 
        data: { 
            xid_venta: id_venta 
        }, 
        url: 'apost_eliminar_retventa.php',
        success: function (result) {
            if(result == 1){
                alert("Ocurrió un error")
            }else{
                $("#btn-devolv-"+id_venta).remove();
                $("#retencion-head-"+id_venta).html("No");
                $("#retencion-"+id_venta).html("0.00");
            }
            
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
    });
});
    
</script>

    
<?php 

if (isset($_POST['enviar-info'])){
	if(!empty($_POST['nombre-cotiza']) && !empty($_POST['email-cotiza'])){
		$nombre_solicitante=$_POST['nombre-cotiza'];
		$correo_solicitante=$_POST['email-cotiza'];
		$numero_solicitante=$_POST['numero-cotiza'];
		$descripcion="inicio:".$_POST['texto-cotiza'];

		enviarconfirmacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        enviarcotizacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        
        echo "<script>alert('Gracias por pensar en nosotros. Su solicitud de cotización ha sido enviada.')</script>";


	}else{

        echo "<script>alert('Por favor complete los datos requeridos.')</script>";
    }
}

?>
</body>
</html>
