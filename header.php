<?php 
//xph es el tipo de pagina
//xhs es para ver si es administrador

//0 : Inicio
//1 : Ver Delegados
//2 : Acreditación
//3 : Elecciones
//4 : Eventos
//5 : Nosotros
//6 : Comité
//7 : Administrar

//validando las variables de sesion
include 'funciones.php';

session_start();
//Inicializamos variables
$sesioninic = 0;
$tipo_pagina = 0;

$sesioninic  = $_POST['xhs'] ;

//CLASES DE LOS LINKS
$a_class_1= "";
$a_class_2= "";
$a_class_3= "";
$a_class_4= "";
$a_class_5= "";
$a_class_6= "";
$a_class_7= "";

if (isset($_POST['xph'])){
    
    $tipo_pagina = $_POST['xph'] ;
    switch ($tipo_pagina) {
        case 1:
            $a_class_1= "active-nav-a";
            break;
        case 2:
            $a_class_2= "active-nav-a";
            break;
        case 3:
            $a_class_3= "active-nav-a";
            break;
        case 4:
            $a_class_4= "active-nav-a";
            break;
        case 5:
            $a_class_5= "active-nav-a";
            break;
        case 6:
            $a_class_6= "active-nav-a";
            break;
        case 7:
            $a_class_7= "active-nav-a";
            break;
    }

}
?>

<nav class="navbar navbar-fixed-top" role="navigation">

	<div class="container cont-nav">
		<div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-dwn" aria-expanded="false" aria-controls="navbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo-vintragos.jpg" class="nav-img-logo" alt="logo"></a>
		</div>
		<div id="navbar-dwn" class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-middle">
<?php if (isset($_SESSION['id_usuario'])) { ?>


 				<li class="<?php echo $a_class_1; ?>">
                    <a href="registro-ventas.php">Registro de ventas</a>
                </li>
 				<li class="<?php echo $a_class_2; ?>">
                    <a href="historial-ventas.php">Historial de ventas</a>
                </li>
<?php if (($_SESSION['poder_usuario']) > 1) { ?>
                <li class="dropdown <?php echo $a_class_3; ?>">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Modificar <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="<?php echo $a_class_1; ?>"><a href="inventario.php">Inventario</a></li>
    <?php if (($_SESSION['poder_usuario']) > 2) { ?>
                    <li class="<?php echo $a_class_1; ?>"><a href="productos.php">Productos</a></li>
                    <li class="<?php echo $a_class_1; ?>"><a href="tipos.php">Tipos</a></li>
                    <li class="<?php echo $a_class_1; ?>"><a href="marcas.php">Marcas</a></li>
                    <li class="<?php echo $a_class_1; ?>"><a href="medidas.php">Medidas</a></li>
                    <li class="<?php echo $a_class_1; ?>"><a href="proveedores.php">Proveedores</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="<?php echo $a_class_1; ?>"><a href="tiendas.php">Tiendas</a></li>
                    <li class="<?php echo $a_class_1; ?>"><a href="usuarios.php">Usuarios</a></li>
    <?php } ?>              
                  </ul>
                </li>
    <?php if (($_SESSION['poder_usuario']) > 2) { ?>
 				<li class="<?php echo $a_class_4; ?>">
                    <a href="historial-ganancias-perdidas.php">Estado de cuenta</a>
                </li>
 <?php 
                }
        }
                
                ?>
 				<li class="dropdown <?php echo $a_class_5; ?>">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Salida <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class=""><a href="cierre.php">Cierre</a></li>
                    <li class=""><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Salir</a></li>
                  </ul>

                    

                </li>
    
 <?php } ?>
            </ul>
		</div>
	</div>
</nav>