<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    if (($_SESSION['poder_usuario']) == 1) {
        echo "<script> window.location.href='registro-ventas.php';</script>";
    }
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

$msg="";

if(isset($_GET['xstate'])){
    
    if($_GET['xstate'] == 0){ 
        $msg = "<div class='msg-probl'><span class='glyphicon glyphicon-remove'></span> Ocurrió un error al subir el inventario, revise que la tabla se haya llenado correctamente, y suba de nuevo el archivo.</div>";
    }elseif($_GET['xstate'] == 1){
        $msg = "<div class='msg-exitoso'><span class='glyphicon glyphicon-ok'></span> Se subió correctamente el archivo.</div>";
    }elseif($_GET['xstate'] == 2){
        $msg_temp = $_GET['xmsg'];
        $msg = "<div class='msg-probl'><span class='glyphicon glyphicon-remove'></span> Está intentando ingresar un inventario con un código de barras que ya existe en la base de datos. Error: $msg_temp</div>";
    }
    
    
}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-info" href="#agre-inv-mod" data-target="#agre-inv-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">Agregar inventario</button>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div><?php echo $msg; ?></div>
            <table class="table table-striped">
                <thead>
                  <tr>
                    <th >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Id</th>
                    <th>Cod_barras</th>
                    <th>Cod_interno</th>
                    <th>Fec_Reg</th>
                    <th>Fec_Venc</th>
                    <th>Cantidad</th>
                    <th>Tienda</th>
                    <th>Subp_cod</th>
                    <th>Subprod_vendidos</th>
                  </tr>
                </thead>
                <tbody>
                    
<?php 
                    


$listar_inventario = listarinventario();

while ($row=mysqli_fetch_assoc($listar_inventario)) {
    
    $id_inventario =$row['id_inventario'];
    $cod_barras =$row['cod_barras'];
    $cod_interno =$row['cod_interno'];
    $fec_ingreso_inventario =$row['fec_ingreso_inventario'];
    $fec_vencimiento_inventario =$row['fec_vencimiento_inventario'];
    $cantidad_inventario =$row['cantidad_inventario'];
    $id_tienda_inventario =$row['id_tienda_inventario'];
    $cod_barras_subproducto =$row['cod_barras_subproducto'];
    $cant_subinv_vend =$row['cant_subinv_vend'];

    $fec_ingreso_inventario = date_create($fec_ingreso_inventario);
    $fec_ingreso_inventario = date_format($fec_ingreso_inventario,"Y-m-d");
    
    $fec_vencimiento_inventario = date_create($fec_vencimiento_inventario);
    $fec_vencimiento_inventario = date_format($fec_vencimiento_inventario,"Y-m-d");
    
                
    $btn_eliminar = "<a class='remover-btn' href='#elim-inv-mod' data-target='#elim-inv-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$id_inventario' data-id='$id_inventario' data-descr='$cod_interno'><span class='glyphicon glyphicon-remove'></span></a>";   
                
    $btn_editar = "<a class='editar-btn' href='#edit-inv-mod' data-target='#edit-inv-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id='$id_inventario'><span class='glyphicon glyphicon-pencil'></span></a>";    
                    
?>
                  <tr id="<?php echo $id_inventario; ?>">
                    <td id="0-<?php echo $id_inventario; ?>"><?php echo $btn_eliminar; ?><?php echo $btn_editar; ?></td>
                    <td id="a-<?php echo $id_inventario; ?>"><?php echo $id_inventario; ?></td>
                    <td id="b-<?php echo $id_inventario; ?>"><?php echo $cod_barras; ?></td>
                    <td id="c-<?php echo $id_inventario; ?>"><?php echo $cod_interno; ?></td>
                    <td id="d-<?php echo $id_inventario; ?>"><?php echo $fec_ingreso_inventario; ?></td>
                    <td id="e-<?php echo $id_inventario; ?>"><?php echo $fec_vencimiento_inventario; ?></td>
                    <td id="f-<?php echo $id_inventario; ?>"><?php echo $cantidad_inventario; ?></td>
                    <td id="g-<?php echo $id_inventario; ?>"><?php echo $id_tienda_inventario; ?></td>
                    <td id="h-<?php echo $id_inventario; ?>"><?php echo $cod_barras_subproducto; ?></td>
                    <td id="i-<?php echo $id_inventario; ?>"><?php echo $cant_subinv_vend; ?></td>
                  </tr>
<?php 
}    
                    
?>    
                    
                </tbody>
              </table>
        </div>
    </div>
</div>

    
<div id="agre-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar inventario</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Descargue el siguiente documento, llénelo con el nuevo inventario, selecciónelo y guárdelo.</label>
                            <div class="link-docu">
                                <a href="documentoej/inventario.csv" dowload="">
                                    <span class="glyphicon glyphicon-circle-arrow-down"></span> INVENTARIO
                                </a>
                            </div>
                        </div>
                     </div>
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Agregar inventario</label>
                            <label for="file"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                            <div id="nombrefoto"></div>
                              <input type="file" class="archivo-subir" id="file" name="file" style="display:none" data-id="1">
                            <span class="span-foto-estado" id="estado-cambio-1"></span>
                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="importSubmit" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<div id="elim-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar inventario</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar el inventario: " <span id="inv-dscr"></span>"?</div>
                <div class=""><b>IMPORTANTE: Si elimina el inventario podría generar problemas.</b></div>
                <div class="mensaje-opc-elim"></div>
                <div class="inv-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elim-inv-btn" data-id="0" data-tr="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="edit-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Editar inventario</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                
                 <form class="form-horizontal form-registdel" role="form" action="" method="post"  enctype="multipart/form-data">
                    <div class="form-group row" style="display:none">
                      <label for="id-id" class="col-sm-4 col-form-label">Id</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Id del inventario" name="id-inv" id="id-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cod-inv" class="col-sm-4 col-form-label">Cod_barras</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Código de barras" name="cod-inv" id="cod-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="codint-inv" class="col-sm-4 col-form-label">Cod_interno</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Código interno" name="codint-inv" id="codint-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="fecven-inv" class="col-sm-4 col-form-label">Fec_venc
                          <br>(AAAA-MM-DD)</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Fecha de vencimiento" name="fecven-inv" id="fecven-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cant-inv" class="col-sm-4 col-form-label">Cantidad</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Cantidad" name="cant-inv" id="cant-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="tienda-inv" class="col-sm-4 col-form-label">Tienda</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Tienda" name="tienda-inv" id="tienda-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="codsub-inv" class="col-sm-4 col-form-label">Cód. subpr.</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Código subproducto" name="codsub-inv" id="codsub-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cantsub-inv" class="col-sm-4 col-form-label">Cant. subpr. vend.</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Cantidad de subproductos vendidos" name="cantsub-inv" id="cantsub-inv">
                      </div>
                    </div>
                     <div class="form-group row">
                          <label for="" class="col-sm-4 col-form-label"></label>
                          <div class="col-sm-8">
                            <button type="submit" class="btn btn-sub-regdel btn-sm" name="camb-inv-sub" onclick="$('#cargando-reg').show()">Guardar</button>
                          </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-reg" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div> 
    
<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 3,
       xhs: 0
   });
    
});


$(document).on("click", '.remover-btn',function(){
    
    var id_opc=$(this).attr('data-id');
    var tr_id=$(this).attr('data-tr');
    var dscr_opc=$(this).attr('data-descr');
    
    
    $("#inv-dscr").html(dscr_opc);
    $(".elim-inv-btn").attr('data-id',id_opc);
    $(".elim-inv-btn").attr('data-tr',tr_id);

});

$(document).on("click", '.editar-btn',function(){
    
    var id_inv=$(this).attr('data-id');
    var codbarr_inv=$("#b-"+id_inv).html();
    var codint_inv=$("#c-"+id_inv).html();
    var fecregi_inv=$("#d-"+id_inv).html();
    var fecvenc_inv=$("#e-"+id_inv).html();
    var cant_inv=$("#f-"+id_inv).html();
    var tienda_inv=$("#g-"+id_inv).html();
    var subpr_inv=$("#h-"+id_inv).html();
    var subprven_inv=$("#i-"+id_inv).html();
    
    
    $("#id-inv").val(id_inv);
    $("#cod-inv").val(codbarr_inv);
    $("#codint-inv").val(codint_inv);
    $("#fecven-inv").val(fecvenc_inv);
    $("#cant-inv").val(cant_inv);
    $("#tienda-inv").val(tienda_inv);
    $("#codsub-inv").val(subpr_inv);
    $("#cantsub-inv").val(subprven_inv);

});

$(document).on("click", '.elim-inv-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    var i1_2 = $(this).attr('data-tr');
    //console.log(i1_1);
    //console.log(i1_2);
    
    $(".mensaje-opc-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_eliminar_inventario.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elim-inv-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar el inventario. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminó el inventario con éxito.");
                $( "#"+i1_2).remove();
            }else if(result == 3){
                alert("El sistema no ha identificado el dominio como Vintragos. Por favor, contáctese con servicio técnico.");
            }else if(result == 4){
                alert("No se ha podido actualizar el documento de lectura. Por favor elimine los archivos y agréguelos de nuevo.");
            }else{

            }
            $(".mensaje-opc-elim").html("");
            
        }
    });


});
</script>

    
<?php 

    
if(isset($_POST['importSubmit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            $temp= 0;
            
            $cmdsql="INSERT INTO `inventario`(`id_inventario`, `cod_barras`, `cod_interno`, `fec_ingreso_inventario`, `fec_vencimiento_inventario`, `cantidad_inventario`, `id_tienda_inventario`, `cod_barras_subproducto`, `cant_subinv_vend`) VALUES ";
            
            while(($line = fgetcsv($csvFile)) !== FALSE){
                $line[0] = utf8_encode($line[0]);
                $line[1] = utf8_encode($line[1]);
                $line[2] = utf8_encode($line[2]);
                $line[3] = utf8_encode($line[3]);
                $line[4] = utf8_encode($line[4]);
                $line[5] = utf8_encode($line[5]);
                $line[6] = utf8_encode($line[6]);
                
                
                try {
                    $line[2] = DateTime::createFromFormat('d/m/Y', $line[2]);
                } catch (Exception $e) {
                    $line[2] = DateTime::createFromFormat('Y-m-d', $line[2]);
                }
                
                $line[2] = date_format($line[2],"Y-m-d");
                
                if(!$line[6]){
                    $line[6] = 0;
                }
                
                if($temp == 0){
                    $cmdsql.= "(NULL, '".$line[0]."', '".$line[1]."', NOW(), '".$line[2]."', $line[3], $line[4], '".$line[5]."', '".$line[6]."')";
                }else{
                    //insert member data into database
                    $cmdsql.= ", (NULL, '".$line[0]."', '".$line[1]."', NOW(), '".$line[2]."', $line[3], $line[4], '".$line[5]."', '".$line[6]."')";
                }
                
                $temp++;
            }
            //echo "<script>console.log('$cmdsql')</script>";
            
            $link=conectar();
            //close opened csv file
            fclose($csvFile);
            
            $resultado=mysqli_query($link, $cmdsql);
            $mensaje= "";
            if(!$resultado){
                //echo "<br>Error de procedimiento";
                $msg_tmp = mysqli_error($link);
                $error_nro = mysqli_errno($link);
                if($error_nro!=1062){
                    echo "<script> window.location.href='inventario.php?xstate=0';</script>";
                }else{
                    echo "<script> window.location.href='inventario.php?xstate=2&xmsg=$msg_tmp';</script>";
                }
            }else{
                $estado_renovararch = renovararchivos();
                if($estado_renovararch=="Correcto"){
                    echo "<script> window.location.href='inventario.php?xstate=1';</script>";
                    echo "<script>console.log('$primer_id_insertado')</script>";
                }else{
                    echo "<script> window.location.href='inventario.php?xstate=3';</script>";
                }
            }

        }else{
            echo "<script>alert('Ocurrió algún error al subir el archivo, por favor intente de nuevo.')</script>";
        }
    }else{
        echo "<script>alert('Por favor suba un archivo CSV.')</script>";
    }
}

    
if(isset($_POST['camb-inv-sub'])){

    if(!empty($_POST['id-inv']) && !empty($_POST['cod-inv']) && !empty($_POST['codint-inv']) && !empty($_POST['fecven-inv']) && !empty($_POST['cant-inv']) && !empty($_POST['tienda-inv'])){
        
        $id_inv = $_POST['id-inv'];
        $cod_inv = $_POST['cod-inv'];
        $codint_inv = $_POST['codint-inv'];
        $fecven_inv = $_POST['fecven-inv'];
        $cant_inv = $_POST['cant-inv'];
        $tienda_inv = $_POST['tienda-inv'];
        $codsub_inv = $_POST['codsub-inv'];
        $cantsub_inv = $_POST['cantsub-inv'];

        $fecven_inv = DateTime::createFromFormat('Y-m-d', $fecven_inv);

        $fecven_inv = date_format($fecven_inv,"Y-m-d");
        
        
        $mensaje_result = actualizarinventario($id_inv, $cod_inv, $codint_inv, $fecven_inv, $cant_inv, $tienda_inv, $codsub_inv, $cantsub_inv);
        
        if(!$mensaje_result){
            echo $mensaje_result;
            echo "<script>alert('Hubo un error al actulizar el inventario. Por favor intenta más tarde.')</script>";
            exit();
        }else{

            $estado_renovararch = renovararchivos();
            if($estado_renovararch=="Correcto"){
                echo "<script>alert('Se actualizó el inventario.')</script>";
                echo "<script> window.location.href='inventario.php';</script>";
            }else{
                echo "<script> window.location.href='inventario.php?xstate=3';</script>";
            }
        }
        
    }else{
        echo "<script>alert('Debe ingresar los datos requeridos.');</script>";
        exit();
    }

}
    
?>
</body>
</html>
