<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {    
    if (($_SESSION['poder_usuario']) <= 2) {
        echo "<script> window.location.href='registro-ventas.php';</script>";
    }
    
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];

}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

$msg="";

if(isset($_GET['xstate'])){
    
    if($_GET['xstate'] == 0){ 
        $msg = "<div class='msg-probl'><span class='glyphicon glyphicon-remove'></span> Ocurrió un error al subir los usuarios, revise que la tabla se haya llenado correctamente, y suba de nuevo el archivo.</div>";
    }elseif($_GET['xstate'] == 1){
        $msg = "<div class='msg-exitoso'><span class='glyphicon glyphicon-ok'></span> Se subió correctamente el archivo.</div>";
    }elseif($_GET['xstate'] == 2){
        $msg = "<div class='msg-probl'><span class='glyphicon glyphicon-remove'></span> Está intentando ingresar usuarios que ya existen en la base de datos.</div>";
    }
    
}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <button class="btn btn-info" href="#agre-inv-mod" data-target="#agre-inv-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin">Agregar usuarios</button>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div><?php echo $msg; ?></div>
            <table class="table table-striped table-responsive table-grande">
                <thead>
                  <tr>
                    <th >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Id</th>
                    <th>Nombres</th>
                    <th>Apellidos</th>
                    <th>Tienda</th>
                    <th>Codigo</th>
                    <th>Contraseña</th>
                    <th>Turno</th>
                    <th>Poder</th>
                    <th>Último ingreso</th>
                    <th>Última salida</th>
                  </tr>
                </thead>
                <tbody>
                    
<?php 
                    


$listar_usuarios = listarusuarios();

while ($row=mysqli_fetch_assoc($listar_usuarios)) {
    
    $id_usuario = $row['id_usuario'];
    $nombres_usuario = $row['nombres_usuario'];
    $apellidos_usuario = $row['apellidos_usuario'];
    $id_tienda_usuario = $row['id_tienda_usuario'];
    $codigo_usuario = $row['codigo_usuario'];
    $pass_usuario = $row['pass_usuario'];
    $turno_usuario = $row['turno_usuario'];
    $poder_usuario= $row['poder_usuario'];
    $ult_ingreso_usuario = $row['ult_ingreso_usuario'];
    $ult_salida_usuario = $row['ult_salida_usuario'];
    
    $ult_ingreso_usuario = date_create($ult_ingreso_usuario);
    $ult_ingreso_usuario = date_format($ult_ingreso_usuario,"Y-m-d H:i:s");
    
    $ult_salida_usuario = date_create($ult_salida_usuario);
    $ult_salida_usuario = date_format($ult_salida_usuario,"Y-m-d H:i:s");
                
    $btn_eliminar = "<a class='remover-btn' href='#elim-inv-mod' data-target='#elim-inv-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-tr='$id_usuario' data-id='$id_usuario' data-descr='$nombres_usuario $apellidos_usuario'><span class='glyphicon glyphicon-remove'></span></a>";   
                
    $btn_editar = "<a class='editar-btn' href='#edit-inv-mod' data-target='#edit-inv-mod' data-toggle='modal' aria-expanded='false' aria-controls='collaplogin' data-id='$id_usuario'><span class='glyphicon glyphicon-pencil'></span></a>";    
                    
?>
                  <tr id="<?php echo $id_usuario; ?>">
                    <td id="0-<?php echo $id_usuario; ?>"><?php echo $btn_eliminar; ?><?php echo $btn_editar; ?></td>
                    <td id="a-<?php echo $id_usuario; ?>"><?php echo $id_usuario; ?></td>
                    <td id="b-<?php echo $id_usuario; ?>"><?php echo $nombres_usuario; ?></td>
                    <td id="c-<?php echo $id_usuario; ?>"><?php echo $apellidos_usuario; ?></td>
                    <td id="d-<?php echo $id_usuario; ?>"><?php echo $id_tienda_usuario; ?></td>
                    <td id="e-<?php echo $id_usuario; ?>"><?php echo $codigo_usuario; ?></td>
                    <td id="f-<?php echo $id_usuario; ?>"><?php echo $pass_usuario; ?></td>
                    <td id="g-<?php echo $id_usuario; ?>"><?php echo $turno_usuario; ?></td>
                    <td id="h-<?php echo $id_usuario; ?>"><?php echo $poder_usuario; ?></td>
                    <td id="i-<?php echo $id_usuario; ?>"><?php echo $ult_ingreso_usuario; ?></td>
                    <td id="j-<?php echo $id_usuario; ?>"><?php echo $ult_salida_usuario; ?></td>
                  </tr>
<?php 
}    
                    
?>    
                    
                </tbody>
              </table>
        </div>
    </div>
</div>

    
<div id="agre-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar usuario</h4>
            </div>
            <div class="modal-body modbod-exp">
                 <form class="form-horizontal form-datoper" role="form" action="" method="post" enctype="multipart/form-data">
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Descargue el siguiente documento, llénelo con los nuevos usuarios, selecciónelo y guárdelo.</label>
                            <div class="link-docu">
                                <a href="documentoej/usuarios.csv" dowload="">
                                    <span class="glyphicon glyphicon-circle-arrow-down"></span> Usuario
                                </a>
                            </div>
                        </div>
                     </div>
                     <div class="row form-datper-left">
                        <div class="col-xs-12">
                            <label class="lbl-login-mod">Agregar usuarios</label>
                            <label for="file"><a class="btn btn-md form-control input-sm selec-arch-mod">Selecciona archivo</a></label>
                            <div id="nombrefoto"></div>
                              <input type="file" class="archivo-subir" id="file" name="file" style="display:none" data-id="1">
                            <span class="span-foto-estado" id="estado-cambio-1"></span>
                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-datper-mod btn-sm" name="importSubmit" onclick="$('#cargando-regpro').show()" >Guardar</button>
                                <button type="button" class="btn btn-datper-canc btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-regpro" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div>
    
<div id="elim-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Eliminar usuario</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                <div class="vota-modbod-msg">¿Está seguro que desea eliminar el usuario: " <span id="inv-dscr"></span>"?</div>
                <div class=""><b>IMPORTANTE: Si elimina el usuario podría generar problemas.</b></div>
                <div class="mensaje-opc-elim"></div>
                <div class="inv-modbod-btn">
                    <button type="button" class="btn btn-sm btn-warning elim-inv-btn" data-id="0" data-tr="0">Si</button>
                    <button type="button" class="btn btn-datper-canc btn-sm btn-info" data-dismiss="modal" aria-label="Close">No</button>
                </div>
            </div>

        </div>
    </div>
</div> 
    
<div id="edit-inv-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Editar usuario</h4>
            </div>
            <div class="modal-body vota-mod-bod">
                
                 <form class="form-horizontal form-registdel" role="form" action="" method="post"  enctype="multipart/form-data">
                    <div class="form-group row" style="display:none">
                      <label for="id-id" class="col-sm-4 col-form-label">Id</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Id del inventario" name="id-inv" id="id-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="nomb-inv" class="col-sm-4 col-form-label">Nombres</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Nombres" name="nomb-inv" id="nomb-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="ape-inv" class="col-sm-4 col-form-label">Apellidos</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Apellidos" name="ape-inv" id="ape-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="tienda-inv" class="col-sm-4 col-form-label">Tienda</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Tienda" name="tienda-inv" id="tienda-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cod-inv" class="col-sm-4 col-form-label">Código</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Código" name="cod-inv" id="cod-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="cont-inv" class="col-sm-4 col-form-label">Contraseña</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="text" placeholder="Contraseña" name="cont-inv" id="cont-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="turno-inv" class="col-sm-4 col-form-label">Turno</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Turno" name="turno-inv" id="turno-inv">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="poder-inv" class="col-sm-4 col-form-label">Poder</label>
                      <div class="col-sm-8">
                        <input class="form-control" type="number" placeholder="Turno" name="poder-inv" id="poder-inv">
                      </div>
                    </div>
                     <div class="form-group row">
                          <label for="" class="col-sm-4 col-form-label"></label>
                          <div class="col-sm-8">
                            <button type="submit" class="btn btn-sub-regdel btn-sm" name="camb-inv-sub" onclick="$('#cargando-reg').show()">Guardar</button>
                          </div>
                     </div>
                     <div class="form-group row">
                         <span id="cargando-reg" style="display:none">Cargando...</span>
                     </div>
                </form>
            </div>

        </div>
    </div>
</div> 
    
<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 3,
       xhs: 0
   });
    
    
    $( ".monto-total-tbl" ).each(function( index ) {
        var data_id = $(this).attr("data-id");
        var monto_tot = $(this).html();
        $("#monto-headtot-"+data_id).html(monto_tot);
    });
});


$(document).on("click", '.btn-dev-ret',function(){
    var id_venta = $(this).attr("data-id");
    $.ajax({
        type: 'post', 
        data: { 
            xid_venta: id_venta 
        }, 
        url: 'apost_eliminar_retventa.php',
        success: function (result) {
            if(result == 1){
                alert("Ocurrió un error")
            }else{
                $("#btn-devolv-"+id_venta).remove();
                $("#retencion-head-"+id_venta).html("No");
                $("#retencion-"+id_venta).html("0.00");
            }
            
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
    });
});

$(document).on("click", '.remover-btn',function(){
    
    var id_opc=$(this).attr('data-id');
    var tr_id=$(this).attr('data-tr');
    var dscr_opc=$(this).attr('data-descr');
    
    
    $("#inv-dscr").html(dscr_opc);
    $(".elim-inv-btn").attr('data-id',id_opc);
    $(".elim-inv-btn").attr('data-tr',tr_id);

});

$(document).on("click", '.editar-btn',function(){
    
    var id_inv=$(this).attr('data-id');
    var nomb_inv=$("#b-"+id_inv).html();
    var ape_inv=$("#c-"+id_inv).html();
    var tienda_inv=$("#d-"+id_inv).html();
    var cod_inv=$("#e-"+id_inv).html();
    var cont_inv=$("#f-"+id_inv).html();
    var turno_inv=$("#g-"+id_inv).html();
    var poder_inv=$("#h-"+id_inv).html();
    
    
    $("#id-inv").val(id_inv);
    $("#nomb-inv").val(nomb_inv);
    $("#ape-inv").val(ape_inv);
    $("#tienda-inv").val(tienda_inv);
    $("#cod-inv").val(cod_inv);
    $("#cont-inv").val(cont_inv);
    $("#turno-inv").val(turno_inv);
    $("#poder-inv").val(poder_inv);

});

$(document).on("click", '.elim-inv-btn',function(){
    
    var i1_1 = $(this).attr('data-id');
    var i1_2 = $(this).attr('data-tr');
    //console.log(i1_1);
    //console.log(i1_2);
    
    $(".mensaje-opc-elim").html("Eliminando...");
    
    $.ajax({
        url:'apost_eliminar_usuario.php',
        type:'post',
        data:{
            'x_i1': i1_1
        },
        success: function (result) {
            $('#elim-inv-mod').modal('hide');
            if(result == 1){
                //console.log(result);
                alert("Ocurrió algún problema al eliminar el usuario. Si este inconveniente persiste, comuníquese con servicio técnico.");
            }else if(result == 2){
                alert("Se eliminó el usuario con éxito.");
                $( "#"+i1_2).remove();
            }else if(result == 3){
                alert("El sistema no ha identificado el dominio como Vintragos. Por favor, contáctese con servicio técnico.");
            }else{

            }
            $(".mensaje-opc-elim").html("");
            
        }
    });


});
</script>

    
<?php 

    
if(isset($_POST['importSubmit'])){
    //validate whether uploaded file is a csv file
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            //open uploaded csv file with read only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            //skip first line
            fgetcsv($csvFile);
            
            //parse data from csv file line by line
            $temp= 0;
            
            $cmdsql="INSERT INTO `usuario`(`id_usuario`, `nombres_usuario`, `apellidos_usuario`, `id_tienda_usuario`, `codigo_usuario`, `pass_usuario`, `turno_usuario`, `poder_usuario`, `ult_ingreso_usuario`, `ult_salida_usuario`) VALUES ";
            
            while(($line = fgetcsv($csvFile)) !== FALSE){
                $line[0] = utf8_encode($line[0]);
                $line[1] = utf8_encode($line[1]);
                $line[2] = utf8_encode($line[2]);
                $line[3] = utf8_encode($line[3]);
                $line[4] = utf8_encode($line[4]);
                $line[5] = utf8_encode($line[5]);
                $line[6] = utf8_encode($line[6]);
                
                if($temp == 0){
                    $cmdsql.= "(NULL, '".$line[0]."', '".$line[1]."', $line[2], '".$line[3]."', '".$line[4]."', '".$line[5]."', '".$line[6]."', NOW(), NOW())";
                }else{
                    //insert member data into database
                    $cmdsql.= ", (NULL, '".$line[0]."', '".$line[1]."', $line[2], '".$line[3]."', '".$line[4]."', '".$line[5]."', '".$line[6]."', NOW(), NOW())";
                }
                
                $temp++;
            }
            //echo $cmdsql;
            
            $link=conectar();
            //close opened csv file
            fclose($csvFile);
            
            $resultado=mysqli_query($link, $cmdsql);
            $mensaje= "";
            if(!$resultado){
                //echo "<br>Error de procedimiento";
                //echo mysqli_error($link);
                $error_nro = mysqli_errno($link);
                if($error_nro!=1062){
                    echo "<script> window.location.href='usuarios.php?xstate=0';</script>";
                }else{
                    echo "<script> window.location.href='usuarios.php?xstate=2';</script>";
                }
            }else{
                echo "<script> window.location.href='usuarios.php?xstate=1';</script>";
                echo "<script>console.log('$primer_id_insertado')</script>";
            }

        }else{
            echo "<script>alert('Ocurrió algún error al subir el archivo, por favor intente de nuevo.')</script>";
        }
    }else{
        echo "<script>alert('Por favor suba un archivo CSV.')</script>";
    }
}

    
if(isset($_POST['camb-inv-sub'])){

    if(!empty($_POST['id-inv']) && !empty($_POST['nomb-inv'])  && !empty($_POST['ape-inv']) && !empty($_POST['tienda-inv']) && !empty($_POST['cod-inv']) && !empty($_POST['cont-inv'])  && !empty($_POST['turno-inv'])   && !empty($_POST['poder-inv']) ){
        
        $id_inv = $_POST['id-inv'];
        $nomb_inv = $_POST['nomb-inv'];
        $ape_inv = $_POST['ape-inv'];
        $tienda_inv = $_POST['tienda-inv'];
        $cod_inv = $_POST['cod-inv'];
        $cont_inv = $_POST['cont-inv'];
        $turno_inv = $_POST['turno-inv'];
        $poder_inv = $_POST['poder-inv'];
        
                
        $mensaje_result = actualizarusuario($id_inv, $nomb_inv, $ape_inv, $tienda_inv, $cod_inv, $cont_inv, $turno_inv, $poder_inv);
        
        if(!$mensaje_result){
            echo $mensaje_result;
            echo "<script>alert('Hubo un error al actualizar el usuario. Por favor intenta más tarde.')</script>";
            exit();
        }else{
            echo "<script>alert('Se actualizó el usuario.')</script>";
            echo "<script> window.location.href='usuarios.php';</script>";
        }
        
    }else{
        echo "<script>alert('Debe ingresar los datos requeridos.');</script>";
        exit();
    }

}
    
?>
</body>
</html>
