<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
    
}else{
    
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";*/
    
    
    echo "<script> window.location.href='index.php';</script>";
}

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link rel="stylesheet" href="css/keyboard.css"> 
    <link rel="stylesheet" href="css/keyboard-dark.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-venta-reg">
        <div class="col-xs-12">
            <div class="info-venta-wrapp">
                <div class="info-venta-inn">
                      <table class="table table-striped table-bordered table-venta">
                        <thead>
                          <tr>
                            <th class="th-venta-elim">El.</th>
                            <th class="th-venta-cod">Código</th>
                            <th class="th-venta-cant">Cant.</th>
                            <th class="th-venta-subp">Subp.</th>
                            <th class="th-venta-punit">P. unit</th>
                            <th class="th-venta-ptot">P. tot</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="tr-venta-descuento">
                            <td class="td-venta-vacio" colspan="4"><button class="btn btn-info btn-agre-prod" href="#agregar-prod-mod" data-target="#agregar-prod-mod" data-toggle="modal" aria-expanded="false" aria-controls="collaplogin" >Agregar producto</button></td>
                            <td class="td-venta-desc">Descuento</td>
                            <td class="td-venta-descuento"><input type="number" step="0.50" class="form-control osk-trigger" id="descuento-venta"></td>
                          </tr>
                          <tr class="tr-venta-monto">
                            <td class="td-venta-vacio" colspan="4"></td>
                            <td class="td-venta-total">Total</td>
                            <td class="td-venta-montotot" id="td-venta-montotot">0.00</td>
                          </tr>
                          <tr class="tr-venta-paga">
                            <td class="td-venta-vacio" colspan="4"></td>
                            <td class="td-venta-paga">Paga</td>
                            <td class="td-venta-pagamonto"><input type="number" step="0.50" class="form-control osk-trigger" id="paga-venta"></td>
                          </tr>
                          <tr class="tr-venta-vuelto">
                            <td class="td-venta-vacio" colspan="4"></td>
                            <td class="td-venta-vuelto">Vuelto</td>
                            <td class="td-venta-vueltomonto">0.00</td>
                          </tr>
                        </tbody>
                      </table>
                </div>
            </div>
            <div class="infoad-venta-wrapp">
                <div class="infoad-venta-inn form-inline">
                    <div class="form-group retencion-wrapp">
                        <label for="retencion">Retención:</label>
                        <input type="number" step="0.50" class="form-control osk-trigger" id="retencion" value="">
                    </div>                  
                    <div class="form-group adicional-wrapp">
                        <label for="adicional">Adicional:</label>
                        <input type="text" class="form-control osk-trigger" id="adicional">
                    </div>          
                </div>
            </div>
            <div class="btn-subirventa-wrapp">
                <div class="btn-subirventa-inn">
                    <button class="btn btn-success btn-realizar-venta">Realizar venta</button>
                </div>
            </div>
        </div>
    </div>
</div>
    

<div id="footer"></div>

<div id="agregar-prod-mod" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close" style="border: 2px solid #000;">&times;</button>
                <h4 class="modal-title">Agregar producto</h4>
            </div>
            <div class="modal-body modbod-agprod">
                 <div class="form-horizontal form-agreprod">
                     <div class="row form-datper-left">
                        <div class="col-xs-12" id="autocompletar-wrapp">
                            <label class="lbl-login-mod" for="nuevo-prod">Código de barras:</label>
                            <input type="text" class="form-control osk-trigger" id="nuevo-prod" autocomplete="off">
                        </div>
                     </div>
                     <div class="row">
                         <div class="col-xs-12 datpersbut-wrap">
                              <div class="datpersbut-mod">
                                <button type="submit" class="btn btn-info btn-sm agregar-prod-btn">Agregar</button>
                                <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal" aria-label="Close">Cancelar</button>
                             </div>
                         </div>
                     </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="js/jquery-bootstrap-mix.js"></script>
<script type="text/javascript" src="js/jquery.scannerdetection.js"></script>
<script src="js/jquery.easy-autocomplete.min.js"></script>
<script src="js/jquery.keyboard.js"></script>
<script src="js/jquery.mousewheel.js"></script>
<script src="js/jquery.keyboard.extension-typing.js"></script>
<script src="js/jquery.keyboard.extension-autocomplete.js"></script>
<script src="js/jquery.keyboard.extension-caret.js"></script>
<script src="js/princ.js"></script>
<script src="js/touch.js"></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;
var myjson;
$(document).on('hidden.bs.modal', '#agregar-prod-mod', function (event){
    //console.log(event);
    if(!$("#agregar-prod-mod").is(":visible")){
        $("#nuevo-prod").val('');
    }
    
});
    
$(function(){
   $("#header").load("header.php", {
       xph: 1,
       xhs: 0
   });
    $.getJSON("database/database-"+id_tienda+".json", function(json){
        myjson = json;
        

        $(document).scannerDetection({
            timeBeforeScanTest: 400, // wait for the next character for upto 200ms
            startChar: [120], // Prefix character for the cabled scanner (OPL6845R)
            endChar: [13], // be sure the scan is complete if key 13 (enter) is detected
            avgTimeByChar: 200, // it's not a barcode if a character takes longer than 40ms
            onComplete: function(barcode, qty){ 
            console.log("código de barras: "+barcode); 
                //console.log(qty);

            var codigo_interno ="";
            var codigo_interno_subproducto ="";
            var id_inventario =0;
            var precio_venta =0;
            var descuento_venta =0;
            var precio_venta_final =0;
            var nuevo_elemento_tr = "";

            var result = $.grep(myjson.inventario, function(element, index) {
               return ((element.cod_barras == barcode) && (element.id_tienda_inventario == id_tienda));
            });
            if(result[0]){
                codigo_interno =result[0].cod_interno;
                id_inventario =result[0].id_inventario;
                console.log(codigo_interno);

                var result = $.grep(myjson.producto, function(element, index) {
                   return (element.cod_interno == codigo_interno);
                });

                if(result[0]){
                    precio_venta =result[0].prec_venta_producto;
                    precio_compra =result[0].prec_compra_producto;
                    descuento_venta =result[0].descuento_producto;
                    cant_subproducto =result[0].cantidad_subproducto;
                    codigo_interno_subproducto =result[0].cod_interno_subproducto;

                    precio_venta_final=parseFloat(precio_venta) -parseFloat(descuento_venta);
                    precio_venta_final=precio_venta_final.toFixed(2);

                    console.log(precio_venta_final);
                    nuevo_elemento_tr +="<tr class='tr-venta-detalle' data-id='"+id_data_producto+"'>";
                    nuevo_elemento_tr +="<td class='td-venta-elim' data-id='"+id_data_producto+"'><a><span class='glyphicon glyphicon-remove'></span></a></td>";
                    nuevo_elemento_tr +="<td class='td-venta-cod' data-id='"+id_data_producto+"' data-inventario='"+id_inventario+"'>"+codigo_interno+"</td>";
                    nuevo_elemento_tr +="<td>";
                    nuevo_elemento_tr +="<select class='form-control td-venta-cant' data-id='"+id_data_producto+"'>";
                    for (i = 1; i <= 12; i++) { 
                        nuevo_elemento_tr +="<option class='sel-prov' value='"+i+"'>"+i+"</option>";
                    }
                    nuevo_elemento_tr +="</select>";
                    nuevo_elemento_tr +="</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-venta-subp'>";
                    if(codigo_interno_subproducto != ""){
                        nuevo_elemento_tr +="<div class='form-group'><label class='label-subp-detable'>Si <input type='checkbox' class='input-venta-sub' data-id='"+id_data_producto+"' data-subprod='"+codigo_interno_subproducto+"'></label></div> ";
                    }
                    nuevo_elemento_tr +="</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-venta-punit' data-precio='"+precio_venta_final+"' data-subprecio='0'>"+precio_venta_final+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-compra-punit' data-precio='"+precio_compra+"' data-subprecio='0'>"+precio_compra+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-compra-ptot' data-precio='"+precio_compra+"' data-subprecio='0'>"+precio_compra+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-subp-cant'>"+cant_subproducto+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-venta-ptot'>"+precio_venta_final+"</td>";

                    $(nuevo_elemento_tr).insertBefore(".tr-venta-descuento");
                    actualizarmonto();
                    actualizarvuelto();
                    id_data_producto++;

                }else{
                    console.log("No se encontró producto");
                }

            }else{
                console.log("No se encontró en inventario");
            }
            } // main callback function	
        });

        $(document).on("click", '.agregar-prod-btn',function(){

            var codigo_barras = $("#nuevo-prod").val();
            var codigo_interno_subproducto ="";
            var id_inventario =0;
            var precio_venta =0;
            var descuento_venta =0;
            var precio_venta_final =0;
            var nuevo_elemento_tr = "";
            
            console.log("Codigo de barras: "+codigo_barras);

            var result = $.grep(myjson.inventario, function(element, index) {
               return ((element.cod_barras == codigo_barras) && (element.id_tienda_inventario == id_tienda));
            });
            if(result[0]){
                id_inventario =result[0].id_inventario;
                codigo_interno =result[0].cod_interno;
                console.log(id_inventario);

                var result = $.grep(myjson.producto, function(element, index) {
                   return (element.cod_interno == codigo_interno);
                });

                if(result[0]){
                    precio_venta =result[0].prec_venta_producto;
                    precio_compra =result[0].prec_compra_producto;
                    descuento_venta =result[0].descuento_producto;
                    cant_subproducto =result[0].cantidad_subproducto;
                    codigo_interno_subproducto =result[0].cod_interno_subproducto;

                    precio_venta_final=parseFloat(precio_venta) -parseFloat(descuento_venta);
                    precio_venta_final=precio_venta_final.toFixed(2);

                    console.log(precio_venta_final);
                    nuevo_elemento_tr +="<tr class='tr-venta-detalle' data-id='"+id_data_producto+"'>";
                    nuevo_elemento_tr +="<td class='td-venta-elim' data-id='"+id_data_producto+"'><a><span class='glyphicon glyphicon-remove'></span></a></td>";
                    nuevo_elemento_tr +="<td class='td-venta-cod' data-id='"+id_data_producto+"' data-inventario='"+id_inventario+"'>"+codigo_interno+"</td>";
                    nuevo_elemento_tr +="<td>";
                    nuevo_elemento_tr +="<select class='form-control td-venta-cant' data-id='"+id_data_producto+"'>";
                    for (i = 1; i <= 12; i++) { 
                        nuevo_elemento_tr +="<option class='sel-prov' value='"+i+"'>"+i+"</option>";
                    }
                    nuevo_elemento_tr +="</select>";
                    nuevo_elemento_tr +="</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-venta-subp'>";
                    if(codigo_interno_subproducto != ""){
                        nuevo_elemento_tr +="<div class='form-group'><label class='label-subp-detable'>Si <input type='checkbox' class='input-venta-sub' data-id='"+id_data_producto+"' data-subprod='"+codigo_interno_subproducto+"'></label></div> ";
                    }
                    nuevo_elemento_tr +="</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-venta-punit' data-precio='"+precio_venta_final+"' data-subprecio='0'>"+precio_venta_final+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-compra-punit' data-precio='"+precio_compra+"' data-subprecio='0'>"+precio_compra+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-compra-ptot' data-precio='"+precio_compra+"' data-subprecio='0'>"+precio_compra+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-subp-cant'>"+cant_subproducto+"</td>";
                    nuevo_elemento_tr +="<td data-id='"+id_data_producto+"' class='td-venta-ptot'>"+precio_venta_final+"</td>";

                    $(nuevo_elemento_tr).insertBefore(".tr-venta-descuento");

                    $('#agregar-prod-mod').modal('hide');
                    actualizarmonto();
                    actualizarvuelto();
                    id_data_producto++;

                }else{
                    console.log("No se encontró producto");
                }

            }else{
                console.log("No se encontró en inventario");
            }
        });


    });  
    console.log("se cargó el json");
});
//SUBIR VENTA
$(document).on("click", '.btn-realizar-venta',function(){
    
    $(".row-venta-reg").css({
        'opacity':'0.5',
        'pointer-events':'none'
    });

    //Variables de detalles
    var idsinventario_detalle_venta = new Array();
    var cantidades_detalle_venta = new Array();
    var subproductos_detalle_venta = new Array();
    var cantsubps_detalle_venta = new Array();
    var montos_detalle_venta = new Array();
    var montos_detalle_compra = new Array();
    
    //Variables de venta
    var descuento_detalle_venta = 0;
    var retencion_detalle_venta = 0;
    var adicional_detalle_venta = "";

    $( ".tr-venta-detalle" ).each(function() {
        var id_detalle_venta = $(this).attr("data-id");
        var idinventario_detalle_venta = $(".td-venta-cod[data-id="+id_detalle_venta+"]").attr("data-inventario");
        var cantidad_detalle_venta = $(".td-venta-cant[data-id="+id_detalle_venta+"]").val();
        var subproducto_detalle_venta = $(".input-venta-sub[data-id="+id_detalle_venta+"]").is(':checked');
        var cantidad_subproductos = parseInt($(".td-subp-cant[data-id="+id_detalle_venta+"]").html());
        var cantidad_subproductos_vend = parseInt($(".td-subp-vend[data-id="+id_detalle_venta+"]").html());
        var monto_detalle_venta = $(".td-venta-ptot[data-id="+id_detalle_venta+"]").html();
        var monto_detalle_compra = $(".td-compra-ptot[data-id="+id_detalle_venta+"]").html();
        
        var cantidad_subproductos_vend = 0;
        
        if(subproducto_detalle_venta){
            subproducto_detalle_venta = 1;
            
            //DESDE ACA SE MODIFICA
            //SE VERIFICAN QUE UN MISMO INVENTARIO SE REPITE Y SE SUMAN LOS VALORES
            //DEL INVENTARIO REPETIDO
            //OBTENEMOS LOS SUBPRODUCTOS VENDIDOS
            var result = $.grep(myjson.inventario, function(element, index) {
               return (element.id_inventario == idinventario_detalle_venta);
            });
            if(result[0]){
                cantidad_subproductos_vend = result[0].cant_subinv_vend;
            }
            
            //COMO SON SUBPRODUCTOS, COJO LA CANTIDAD
            console.log("Subproductos actuales:"+ cantidad_subproductos_vend);
            var cantsubp_detalle_venta = parseInt(cantidad_detalle_venta) + parseInt(cantidad_subproductos_vend);
            console.log("Subproductos totales:"+ cantsubp_detalle_venta);
            //LA PARTE ENTERA DE LA DIVISIÓN SE ACTUALIZARÁ AL INVENTARIO
            var cantidad_detalle_venta = Math.floor(cantsubp_detalle_venta/cantidad_subproductos);
            console.log("Productos totales:"+ cantidad_detalle_venta);
            //LO QUE RESTE SE IRÁ AL SUBINVENTARIO
            var cantsubp_detalle_venta = cantsubp_detalle_venta - cantidad_subproductos*cantidad_detalle_venta;
            console.log("Subproductos final:"+ cantsubp_detalle_venta);
            
            var posicion_array = $.inArray( idinventario_detalle_venta, idsinventario_detalle_venta );
            if(posicion_array != -1){
                cantidades_detalle_venta[posicion_array] =  parseInt(cantidades_detalle_venta[posicion_array])+parseInt(cantidad_detalle_venta);
                cantsubps_detalle_venta[posicion_array] =  parseInt(cantsubps_detalle_venta[posicion_array])+parseInt(cantsubp_detalle_venta);
                montos_detalle_venta[posicion_array] =  parseFloat(montos_detalle_venta[posicion_array])+parseFloat(monto_detalle_venta);
             }else{
                idsinventario_detalle_venta.push(idinventario_detalle_venta);
                cantidades_detalle_venta.push(cantidad_detalle_venta);
                subproductos_detalle_venta.push(subproducto_detalle_venta);
                cantsubps_detalle_venta.push(cantsubp_detalle_venta);
                montos_detalle_venta.push(monto_detalle_venta);
                montos_detalle_compra.push(monto_detalle_compra);
             }

            //ACTUALIZAMOS LA VARIABLE DEL JSON
            var result = $.grep(myjson.inventario, function(element, index) {
               return (element.id_inventario == idinventario_detalle_venta);
            });
            if(result[0]){
                result[0].cant_subinv_vend = cantsubp_detalle_venta;
            }
        }else{
            subproducto_detalle_venta = 0;
            
            //DESDE ACA SE MODIFICA
            //SE VERIFICAN QUE UN MISMO INVENTARIO SE REPITE Y SE SUMAN LOS VALORES
            //DEL INVENTARIO REPETIDO
            var cantsubp_detalle_venta = 0;
            var posicion_array = $.inArray( idinventario_detalle_venta, idsinventario_detalle_venta );
            if(posicion_array != -1){
                cantidades_detalle_venta[posicion_array] =  parseInt(cantidades_detalle_venta[posicion_array])+parseInt(cantidad_detalle_venta);
                montos_detalle_venta[posicion_array] =  parseFloat(montos_detalle_venta[posicion_array])+parseFloat(monto_detalle_venta);
             }else{
                idsinventario_detalle_venta.push(idinventario_detalle_venta);
                cantidades_detalle_venta.push(cantidad_detalle_venta);
                subproductos_detalle_venta.push(subproducto_detalle_venta);
                cantsubps_detalle_venta.push(cantsubp_detalle_venta);
                montos_detalle_venta.push(monto_detalle_venta);
                montos_detalle_compra.push(monto_detalle_compra);
             }
            
        }
        

        
    });
    
    descuento_detalle_venta = $("#descuento-venta").val();
    
    if(!descuento_detalle_venta){
        descuento_detalle_venta="0.00";
    }
    
    retencion_detalle_venta = $("#retencion").val();
    
    if(!retencion_detalle_venta){
        retencion_detalle_venta="0.00";
    }
    
    adicional_detalle_venta = $("#adicional").val();
    
    console.log(retencion_detalle_venta);
    console.log(adicional_detalle_venta);
    
    
    $.ajax({
        url:'apost_agregar_venta.php',
        type:'post',
        data:{
            'xidsinventario_detalle_venta':idsinventario_detalle_venta,
            'xcantidades_detalle_venta':cantidades_detalle_venta,
            'xsubproductos_detalle_venta':subproductos_detalle_venta,
            'xcantsubps_detalle_venta':cantsubps_detalle_venta,
            'xmontos_detalle_venta':montos_detalle_venta,
            'xmontos_detalle_compra':montos_detalle_compra,
            'xdescuento_detalle_venta':descuento_detalle_venta,
            'xretencion_detalle_venta':retencion_detalle_venta,
            'xadicional_detalle_venta':adicional_detalle_venta,
            'xidtienda':id_tienda,
            'xid_usuario':id_usuario,
        },
        success: function (result) {
            $(".row-venta-reg").css({
                'opacity':'1',
                'pointer-events':'inherit'
            });
            if(result.includes("Error")){
                console.log(result);
                alert("Ocurrió algún problema. Inténtelo de nuevo.");
            }else{
                console.log(result);
                
                $("#paga-venta, #adicional").val('');
                $("#descuento-venta").val('');
                $("#retencion").val('');
                $(".td-venta-montotot, .td-venta-vueltomonto").html('0.00');
                $(".tr-venta-detalle").remove();   
                
                //2DO AJAX
                
                var id_venta_ingresada = result;
                $.ajax({
                    url:'apost_enviarmail_venta.php',
                    type:'post',
                    data:{
                        'xid_venta_ingresada':id_venta_ingresada
                    },
                    success: function (result) {
                        if(result.includes("Error")){
                            console.log("Error al enviar el mensaje.");
                            console.log(result);
                            //$("body").append(result);
                        }else{
                            console.log("Se envió el mensaje con éxito.");
                            //console.log(result);
                            //$("body").append(result);
                        }
                    }
                });
                
                //END 2DO AJAX
                
            }
        }
    });
    
});
    
/*
var options = {
	url: "database/database.json",
	listLocation: "inventario",
	getValue: "cod_interno",
    list: {
		match: {
			enabled: true
		}
	}

};
    
$("#nuevo-prod").easyAutocomplete(options);
*/
</script>

    
<?php 

if (isset($_POST['enviar-info'])){
	if(!empty($_POST['nombre-cotiza']) && !empty($_POST['email-cotiza'])){
		$nombre_solicitante=$_POST['nombre-cotiza'];
		$correo_solicitante=$_POST['email-cotiza'];
		$numero_solicitante=$_POST['numero-cotiza'];
		$descripcion="inicio:".$_POST['texto-cotiza'];

		enviarconfirmacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        enviarcotizacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        
        echo "<script>alert('Gracias por pensar en nosotros. Su solicitud de cotización ha sido enviada.')</script>";


	}else{

        echo "<script>alert('Por favor complete los datos requeridos.')</script>";
    }
}

?>
</body>
</html>
