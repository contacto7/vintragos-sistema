<?php
session_start();
$sesioninic=0;
$id_tienda=0;
$id_usuario=0;
$nombre_tienda = "";
$nombres_usuario = "";
$apellidos_usuario = "";

//validando las variables de sesion
include 'funciones.php';
if (isset($_SESSION['id_usuario'])) {
    $sesioninic=1;
    $id_tienda = $_SESSION['id_tienda'];
    $id_usuario = $_SESSION['id_usuario'];
    $nombre_tienda = $_SESSION['nombre_tienda'];
	$nombres_usuario = $_SESSION['nombres_usuario'];
	$apellidos_usuario = $_SESSION['apellidos_usuario'];
    $hora_ingreso = $_SESSION['ingreso_usuario'];
}else{
    /*
    $sesioninic=1;
    $id_tienda=1;
    $id_usuario=1;
    $nombre_tienda = "Tienda Upao";
    $nombres_usuario = "José Julio";
    $apellidos_usuario = "Armando Huamán";
    */
    echo "<script> window.location.href='index.php';</script>";
}

$cant_list = 20;
$temp_pag = 0;

if(isset($_GET['xid_last_id'])){
    $last_id_ventas = $_GET['xid_last_id'];
    $temp_pag = $_GET['pag_num'];
    
}else{
    $last_id_ventaslist = ultimoidventa();
    while ($row=mysqli_fetch_assoc($last_id_ventaslist)) {

        $last_id_ventas =$row['id_venta'];
    }
}

$mensaje="";
$mensaje.="<table class='table table-bordered'>";
$mensaje.='<thead>';
$mensaje.='<tr>';
$mensaje.='<th class="th-venta-cod">Código</th>';
$mensaje.='<th class="th-venta-cant">Cant.</th>';
$mensaje.='<th class="th-venta-punit">P. unit</th>';
$mensaje.='<th class="th-venta-ptot">P. tot</th>';
$mensaje.='</tr>';
$mensaje.='</thead>';
$mensaje.='<tbody>';

$listar_ventas_us = listarventasusuarioturno($id_usuario, $hora_ingreso);
$head_ret_list = "";    
$head_adi_list = "";    

$monto_total_lista = 0;
$temp_id=0;
$objetosventa = array();

while ($row=mysqli_fetch_assoc($listar_ventas_us)) {

    $id_venta_list =$row['id_venta'];
    $fec_venta_list =$row['fec_venta'];
    $descuento_venta =$row['descuento_venta'];
    $retencion_envase_venta_list =$row['retencion_envase_venta'];
    $adicional_venta_list =$row['adicional_venta'];
    $cod_interno_list =$row['cod_interno'];
    $cantidad_venta_list =$row['cantidad_venta'];
    $subproducto_venta_list =$row['subproducto_venta'];
    $monto_venta_list =$row['monto_venta'];

    if($subproducto_venta_list){
        $cod_interno_list =$row['cod_interno_subproducto'];
    }

    if($cantidad_venta_list){
        $submonto_venta_list =$monto_venta_list / $cantidad_venta_list;
    }else{
        $cod_interno_list.=" (subproductos)";
        $submonto_venta_list =$monto_venta_list;
    }

    $monto_total_lista+=$monto_venta_list;

    $mensaje.='<tr>';
    $mensaje.="<td>$cod_interno_list</td>";
    $mensaje.="<td>$cantidad_venta_list</td>";
    $mensaje.="<td>$submonto_venta_list</td>";
    $mensaje.="<td>$monto_venta_list</td>";
    $mensaje.="</tr>";

    if( ($id_venta_list != $temp_id) && $descuento_venta>0){
        $mensaje.='<tr>';
        $mensaje.='<td>Descuento</td>';
        $mensaje.='<td>1</td>';
        $mensaje.="<td>$descuento_venta</td>";
        $mensaje.="<td>$descuento_venta</td>";
        $mensaje.='</tr>';

        $monto_total_lista-=$descuento_venta;
    }
    if( ($id_venta_list != $temp_id) && $retencion_envase_venta_list>0){
        $mensaje.='<tr>';
        $mensaje.='<td>Retencion</td>';
        $mensaje.='<td>1</td>';
        $mensaje.="<td>$retencion_envase_venta_list</td>";
        $mensaje.="<td>$retencion_envase_venta_list</td>";
        $mensaje.='</tr>';

        $monto_total_lista+=$retencion_envase_venta_list;
    }

    $temp_id = $id_venta_list;
}
$monto_total_lista=number_format($monto_total_lista, 2, '.', ' ');
$mensaje.='<tr>';
$mensaje.="<td colspan='2'></td>";
$mensaje.="<td>Total S/.</td>";
$mensaje.="<td>$monto_total_lista</td>";
$mensaje.="</tr>";
$mensaje.='</tbody>';
$mensaje.="</table>";

?>
<!DOCTYPE html>
<html ng-app="" lang="es-PE">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv=”Content-Language” content=”es”/>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Vintragos</title>
    <meta name="Title" content="Vintragos">

    <meta name="author" content="www.inoloop.com">
    <!-- 
    <link rel="alternate" hreflang="es-PE" href="https://www.inoloop.com/index.php">
    -->

    <link rel="icon" href="img/logo-vintragos.jpg">
    <link rel="stylesheet" href="css/easy-autocomplete.min.css"> 
    <link href="css/princ.css" rel="stylesheet">
    
</head>
<body>    
<div id="header"></div>

<div class="container container-general">
    <div class="row">
        <div class="col-xs-12">
            <div class="info-tienda-wrapp">
                <div class="info-tienda-inn">
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Usuario:</div>
                        <div class="info-tienda-descr"><?php echo $nombres_usuario." ".$apellidos_usuario; ?></div>
                    </div>
                    <div class="info-tienda-row">
                        <div class="info-tienda-tit">Tienda:</div>
                        <div class="info-tienda-descr"><?php echo $nombre_tienda; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="row">
        <div class="col-xs-12">
            <?php echo $mensaje; ?>
        </div>
    
    </div>
</div>
    

<div id="footer"></div>

<script src="js/jquery-bootstrap-mix.js"></script>
<script src="js/princ.js" async></script>
    
<script>
var id_tienda = <?php echo $id_tienda; ?>;
var id_usuario = <?php echo $id_usuario; ?>;
var id_data_producto = 1;

$('#agregar-prod-mod').on('hidden.bs.modal', function () {
    $("#nuevo-prod").val('');
});
    
$(function(){
    
   $("#header").load("header.php", {
       xph: 5,
       xhs: 0
   });
    
    
    $( ".monto-total-tbl" ).each(function( index ) {
        var data_id = $(this).attr("data-id");
        var monto_tot = $(this).html();
        $("#monto-headtot-"+data_id).html(monto_tot);
    });
});


$(document).on("click", '.btn-dev-ret',function(){
    var id_venta = $(this).attr("data-id");
    $.ajax({
        type: 'post', 
        data: { 
            xid_venta: id_venta 
        }, 
        url: 'apost_eliminar_retventa.php',
        success: function (result) {
            if(result == 1){
                alert("Ocurrió un error")
            }else{
                $("#btn-devolv-"+id_venta).remove();
                $("#retencion-head-"+id_venta).html("No");
                $("#retencion-"+id_venta).html("0.00");
            }
            
        },
        error: function(xhr, status, error) {
          console.log(error);
        }
    });
});
    
</script>

    
<?php 

if (isset($_POST['enviar-info'])){
	if(!empty($_POST['nombre-cotiza']) && !empty($_POST['email-cotiza'])){
		$nombre_solicitante=$_POST['nombre-cotiza'];
		$correo_solicitante=$_POST['email-cotiza'];
		$numero_solicitante=$_POST['numero-cotiza'];
		$descripcion="inicio:".$_POST['texto-cotiza'];

		enviarconfirmacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        enviarcotizacion($nombre_solicitante, $correo_solicitante, $numero_solicitante, $descripcion);
        
        echo "<script>alert('Gracias por pensar en nosotros. Su solicitud de cotización ha sido enviada.')</script>";


	}else{

        echo "<script>alert('Por favor complete los datos requeridos.')</script>";
    }
}

?>
</body>
</html>
