<?php
//error_reporting(E_ALL);
//ini_set('display_errors','on');

include 'funciones.php';

$link = conectar();

if (!isset($_POST['xid_venta_ingresada'])) exit;

$id_venta = $_POST['xid_venta_ingresada'];


$listar_ventas_us = listarventaespecifica($id_venta);
$head_ret_list = "";    
$head_adi_list = "";    

$monto_total_lista = 0;

$objetosventa = array();

while ($row=mysqli_fetch_assoc($listar_ventas_us)) {
    
    $id_venta_list =$row['id_venta'];
    $fec_venta_list =$row['fec_venta'];
    $descuento_venta =$row['descuento_venta'];
    $retencion_envase_venta_list =$row['retencion_envase_venta'];
    $adicional_venta_list =$row['adicional_venta'];
    $cod_interno_list =$row['cod_interno'];
    $cantidad_venta_list =$row['cantidad_venta'];
    $subproducto_venta_list =$row['subproducto_venta'];
    $monto_venta_list =$row['monto_venta'];
    $nombre_tienda_list =$row['nombre_tienda'];
    $nombres_usuario_list =$row['nombres_usuario'];
    $apellidos_usuario_list =$row['apellidos_usuario'];
    
    if($retencion_envase_venta_list > 0){
        $head_ret_list = "Sí";
    }else{
        $head_ret_list = "No";
    }
    if($adicional_venta_list){
        $head_adi_list = "Sí";
    }else{
        $head_adi_list = "No";
    }
    
    $fec_venta_list = date_create($fec_venta_list);
    $fec_venta_list = date_format($fec_venta_list,"d/m/Y H:i:s");
    
    if($subproducto_venta_list){
        $cod_interno_list =$row['cod_interno_subproducto'];
    }
     
    
    if($cantidad_venta_list){
        $submonto_venta_list =$monto_venta_list / $cantidad_venta_list;
    }else{
        $cod_interno_list.=" (subproductos)";
        $submonto_venta_list =$monto_venta_list;
    }

    $tmp = new stdClass;
        
    $tmp->id_venta_list = $id_venta_list;
    $tmp->fec_venta_list = $fec_venta_list;
    $tmp->head_ret_list = $head_ret_list;
    $tmp->descuento_venta = $descuento_venta;
    $tmp->retencion_envase_venta_list = $retencion_envase_venta_list;
    $tmp->adicional_venta_list = $adicional_venta_list;
    $tmp->cod_interno_list = $cod_interno_list;
    $tmp->cantidad_venta_list = $cantidad_venta_list;
    $tmp->subproducto_venta_list = $subproducto_venta_list;
    $tmp->monto_venta_list = $monto_venta_list;
    $tmp->head_ret_list = $head_ret_list;
    $tmp->head_adi_list = $head_adi_list;
    $tmp->submonto_venta_list = $submonto_venta_list;
    $tmp->nombre_tienda_list = $nombre_tienda_list;
    $tmp->nombres_usuario_list = $nombres_usuario_list;
    $tmp->apellidos_usuario_list = $apellidos_usuario_list;

    $objetosventa[$id_venta_list][]=$tmp;
    //rray_push($objetosventa[$id_venta_list],$tmp);
    
    

}

$cantidad_ventas_listadas = 0;
foreach ($objetosventa as $objetoventa) {
    
    $cantidad_ventas_listadas++;
    $monto_total = 0;

    $mensaje="";
    $asunto="";
    
    $mensaje.="<table style='border-collapse: collapse;border: 1px solid black;'><thead><tr><th style='border: 1px solid black;padding:5px'>Código</th><th style='border: 1px solid black;padding:5px'>Cant.</th><th style='border: 1px solid black;padding:5px'>P. unit</th><th style='border: 1px solid black;padding:5px'>P. tot</th></tr></thead><tbody>";

    foreach ($objetoventa as $objetosubventa) {
        $monto_total+=$objetosubventa->monto_venta_list;

        $mensaje.="<tr>";
        $mensaje.="<td style='border: 1px solid black;padding:5px'>$objetosubventa->cod_interno_list</td>";
        $mensaje.="<td style='border: 1px solid black;padding:5px'>$objetosubventa->cantidad_venta_list</td>";
        $mensaje.="<td style='border: 1px solid black;padding:5px'>".number_format($objetosubventa->submonto_venta_list, 2, '.', ' ')."</td>";
        $mensaje.="<td style='border: 1px solid black;padding:5px'>$objetosubventa->monto_venta_list</td>";
        $mensaje.="</tr>";

    }  
    $monto_total-=$objetoventa[0]->descuento_venta;
    
    $mensaje.="<tr><td style='background-color: #fff;border-bottom: 1px solid #fff;border-left: 1px solid #fff;padding:5px' colspan='2'></td>";
    $mensaje.="<td style='border: 1px solid black;padding:5px'>Descuento (S/.)</td>";
    $mensaje.="<td style='border: 1px solid black;padding:5px'>".number_format($objetoventa[0]->descuento_venta, 2, '.', ' ')."</td>";
    $mensaje.="</tr>";
    $mensaje.="<tr><td style='background-color: #fff;border-bottom: 1px solid #fff;border-left: 1px solid #fff;padding:5px' colspan='2'></td>";
    $mensaje.="<td style='border: 1px solid black;padding:5px'>Total (S/.)</td>";
    $mensaje.="<td style='border: 1px solid black;padding:5px'>".number_format($monto_total, 2, '.', ' ')."</td>";
    $mensaje.="</tr>";
    $mensaje.="</tbody>";
    $mensaje.="</table>";
    
    $mensaje.="</br></br><div>";
    $mensaje.="<div>Retención: ".$objetoventa[0]->retencion_envase_venta_list."</div>";
    $mensaje.="<div>Adicional: ".$objetoventa[0]->adicional_venta_list."</div>";
    $mensaje.="</div>";
    
    
    $monto_total_venta = number_format($monto_total, 2, '.', ' ');
    
    $asunto.="S/.";
    $asunto.=$monto_total_venta;
    $asunto.=" - ";
    $asunto.=$objetoventa[0]->nombres_usuario_list;
    $asunto.=" en ";
    $asunto.=$objetoventa[0]->nombre_tienda_list;
}
$correo_nosotros = "vintragosadm@gmail.com";
$nombres_contacto = "Vintragos";
//echo $mensaje;
enviarcorreo($correo_nosotros,$nombres_contacto,$asunto,$mensaje);

?>
